# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: web_schmgmt
# Generation Time: 2023-09-16 09:01:37 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table mkelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mkelas`;

CREATE TABLE `mkelas` (
  `Kd_Kelas` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Kelas` varchar(50) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mkelas` WRITE;
/*!40000 ALTER TABLE `mkelas` DISABLE KEYS */;

INSERT INTO `mkelas` (`Kd_Kelas`, `Nm_Kelas`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'X-1','admin','2020-07-19 06:44:16',NULL,NULL),
	(2,'X-2','admin','2020-07-19 06:44:20',NULL,NULL),
	(3,'X-3','admin','2020-07-19 06:44:25',NULL,NULL),
	(4,'XI-IPA-1','admin','2020-07-19 06:44:32',NULL,NULL),
	(5,'XI-IPA-2','admin','2020-07-19 06:44:38',NULL,NULL),
	(6,'XI-IPS-1','admin','2020-07-19 06:44:46',NULL,NULL),
	(7,'XII-IPA-1','admin','2020-07-19 06:44:53',NULL,NULL),
	(8,'XII-IPA-2','admin','2020-07-19 06:45:00',NULL,NULL),
	(9,'XII-IPS-1','admin','2020-07-19 06:45:09',NULL,NULL);

/*!40000 ALTER TABLE `mkelas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mkomponen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mkomponen`;

CREATE TABLE `mkomponen` (
  `Kd_Komponen` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nm_Komponen` varchar(200) DEFAULT NULL,
  `Bobot` double DEFAULT NULL,
  PRIMARY KEY (`Kd_Komponen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mkomponen` WRITE;
/*!40000 ALTER TABLE `mkomponen` DISABLE KEYS */;

INSERT INTO `mkomponen` (`Kd_Komponen`, `Nm_Komponen`, `Bobot`)
VALUES
	(1,'1. KEHADIRAN',10),
	(2,'2. TUGAS',10),
	(3,'3. ULANGAN',15),
	(4,'4. UTS',30),
	(5,'5. UAS',35);

/*!40000 ALTER TABLE `mkomponen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mmatapelajaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mmatapelajaran`;

CREATE TABLE `mmatapelajaran` (
  `Kd_MataPelajaran` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_MataPelajaran` varchar(50) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_MataPelajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mmatapelajaran` WRITE;
/*!40000 ALTER TABLE `mmatapelajaran` DISABLE KEYS */;

INSERT INTO `mmatapelajaran` (`Kd_MataPelajaran`, `Nm_MataPelajaran`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'MATEMATIKA','admin','2020-07-19 06:48:05',NULL,NULL),
	(2,'BAHASA INDONESIA','admin','2020-07-19 06:48:11',NULL,NULL),
	(3,'BAHASA INGGRIS','admin','2020-07-19 06:48:17',NULL,NULL),
	(5,'PENDIDIKAN OLAHRAGA','admin','2020-07-19 06:49:09','admin','2020-07-19 06:50:06'),
	(6,'KESENIAN','admin','2020-07-19 06:49:15',NULL,NULL),
	(7,'PENDIDIKAN KEWARGANEGARAAN','admin','2020-07-19 06:49:32',NULL,NULL);

/*!40000 ALTER TABLE `mmatapelajaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mpelajar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mpelajar`;

CREATE TABLE `mpelajar` (
  `Kd_Pelajar` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_NomorIndukSiswa` varchar(50) NOT NULL,
  `Nm_Pelajar` varchar(50) NOT NULL,
  `Nm_JenisKelamin` varchar(10) NOT NULL,
  `Nm_Agama` varchar(10) NOT NULL,
  `Nm_Alamat` varchar(200) DEFAULT NULL,
  `Tgl_Lahir` date NOT NULL,
  `Tgl_Masuk` date NOT NULL,
  `Nm_Ayah` varchar(50) DEFAULT NULL,
  `Nm_Ibu` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pelajar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mpelajar` WRITE;
/*!40000 ALTER TABLE `mpelajar` DISABLE KEYS */;

INSERT INTO `mpelajar` (`Kd_Pelajar`, `Nm_NomorIndukSiswa`, `Nm_Pelajar`, `Nm_JenisKelamin`, `Nm_Agama`, `Nm_Alamat`, `Tgl_Lahir`, `Tgl_Masuk`, `Nm_Ayah`, `Nm_Ibu`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'12121111','ANYA GERALDINE','Wanita','',NULL,'0000-00-00','0000-00-00',NULL,NULL,'admin','2020-07-19 06:54:14','admin','2020-07-19 06:54:25'),
	(2,'12122222','ARIEF MUHAMMAD','Pria','',NULL,'0000-00-00','0000-00-00',NULL,NULL,'admin','2020-07-19 06:54:47',NULL,NULL),
	(3,'12123333','BABE CABITA','Pria','',NULL,'0000-00-00','0000-00-00',NULL,NULL,'admin','2020-07-19 06:55:03',NULL,NULL);

/*!40000 ALTER TABLE `mpelajar` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mpengajar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mpengajar`;

CREATE TABLE `mpengajar` (
  `Kd_Pengajar` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_NomorIndukPegawai` varchar(50) NOT NULL,
  `Nm_Pengajar` varchar(50) NOT NULL,
  `Nm_JenisKelamin` varchar(10) NOT NULL,
  `Nm_Alamat` varchar(200) DEFAULT NULL,
  `Tgl_Lahir` date DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pengajar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mpengajar` WRITE;
/*!40000 ALTER TABLE `mpengajar` DISABLE KEYS */;

INSERT INTO `mpengajar` (`Kd_Pengajar`, `Nm_NomorIndukPegawai`, `Nm_Pengajar`, `Nm_JenisKelamin`, `Nm_Alamat`, `Tgl_Lahir`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'111111','KI HAJAR DEWANTARA','Pria',NULL,NULL,'admin','2020-07-19 06:52:27','admin','2020-07-19 06:53:41'),
	(2,'222222','RADEN AJENG KARTINI','Wanita',NULL,NULL,'admin','2020-07-19 06:52:45',NULL,NULL),
	(3,'333333','DR. SOETOMO','Pria',NULL,NULL,'admin','2020-07-19 06:53:13',NULL,NULL),
	(4,'444444','IR. HABIBIE','Pria',NULL,NULL,'admin','2020-07-19 06:53:29',NULL,NULL);

/*!40000 ALTER TABLE `mpengajar` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mperiode
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mperiode`;

CREATE TABLE `mperiode` (
  `Kd_Periode` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Pimpinan` varchar(50) NOT NULL,
  `Nm_WakilPimpinan` varchar(50) NOT NULL,
  `TahunFrom` int(11) NOT NULL,
  `TahunTo` int(11) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Periode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mperiode` WRITE;
/*!40000 ALTER TABLE `mperiode` DISABLE KEYS */;

INSERT INTO `mperiode` (`Kd_Periode`, `Nm_Pimpinan`, `Nm_WakilPimpinan`, `TahunFrom`, `TahunTo`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(2,'T\'Challa','Killmonger',2020,2025,'admin','2020-03-16 16:24:14','admin','2020-03-16 16:28:20');

/*!40000 ALTER TABLE `mperiode` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mprofil
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mprofil`;

CREATE TABLE `mprofil` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Institusi` varchar(50) NOT NULL,
  `Nm_Alamat` varchar(200) DEFAULT NULL,
  `Nm_PhoneNo` varchar(50) DEFAULT NULL,
  `Nm_Email` varchar(50) DEFAULT NULL,
  `Nm_Bio` text,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mprofil` WRITE;
/*!40000 ALTER TABLE `mprofil` DISABLE KEYS */;

INSERT INTO `mprofil` (`Uniq`, `Nm_Institusi`, `Nm_Alamat`, `Nm_PhoneNo`, `Nm_Email`, `Nm_Bio`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'SMA Negeri 1 Wakanda','Wakanda','081211221212','smansa.wakanda@gmail.com','Wakanda Forever..','admin','2020-03-16 17:16:40');

/*!40000 ALTER TABLE `mprofil` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table msesi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `msesi`;

CREATE TABLE `msesi` (
  `Kd_Sesi` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Sesi` varchar(50) NOT NULL,
  `Jam_From` varchar(5) NOT NULL,
  `Jam_To` varchar(5) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Sesi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `msesi` WRITE;
/*!40000 ALTER TABLE `msesi` DISABLE KEYS */;

INSERT INTO `msesi` (`Kd_Sesi`, `Nm_Sesi`, `Jam_From`, `Jam_To`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'SESI 1','07:30','08:30','admin','2020-07-19 06:45:33',NULL,NULL),
	(2,'SESI 2','08:30','09:30','admin','2020-07-19 06:45:46',NULL,NULL),
	(3,'SESI 3','10:00','11:00','admin','2020-07-19 06:45:56','admin','2020-07-19 06:47:19'),
	(4,'SESI 4','11:00','12:00','admin','2020-07-19 06:46:12','admin','2020-07-19 06:47:27'),
	(5,'SESI 5','13:00','14:00','admin','2020-07-19 06:46:40','admin','2020-07-19 06:47:35'),
	(6,'SESI 6','14:00','15:00','admin','2020-07-19 06:46:57','admin','2020-07-19 06:47:43');

/*!40000 ALTER TABLE `msesi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtahunajaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtahunajaran`;

CREATE TABLE `mtahunajaran` (
  `Kd_TahunAjaran` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_TahunAjaran` varchar(50) NOT NULL,
  `Nm_Keterangan` varchar(50) DEFAULT NULL,
  `Is_Active` tinyint(1) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_TahunAjaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mtahunajaran` WRITE;
/*!40000 ALTER TABLE `mtahunajaran` DISABLE KEYS */;

INSERT INTO `mtahunajaran` (`Kd_TahunAjaran`, `Nm_TahunAjaran`, `Nm_Keterangan`, `Is_Active`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'2019 / 2020',NULL,0,'admin','2020-07-19 06:43:30',NULL,NULL),
	(2,'2020 / 2021',NULL,1,'admin','2020-07-19 06:43:39','admin','2020-07-19 06:43:51');

/*!40000 ALTER TABLE `mtahunajaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `postcategories`;

CREATE TABLE `postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `postcategories` WRITE;
/*!40000 ALTER TABLE `postcategories` DISABLE KEYS */;

INSERT INTO `postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`)
VALUES
	(1,'Berita','#f56954'),
	(5,'Others','#3c8dbc');

/*!40000 ALTER TABLE `postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `postimages`;

CREATE TABLE `postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`PostID`, `PostCategoryID`, `PostDate`, `PostTitle`, `PostSlug`, `PostContent`, `PostExpiredDate`, `TotalView`, `LastViewDate`, `IsSuspend`, `FileName`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(4,5,'2019-06-26','Hubungi Kami','hubungi-kami','<p><strong>Dinas Komunikasi Dan Informatika Kabupaten Humbang Hasundutan</strong></p>\r\n\r\n<p>Jl. SM. Raja Kompleks Perkantoran Tano Tubu, Doloksanggul 22457</p>\r\n\r\n<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Telp</td>\r\n			<td>:</td>\r\n			<td>&nbsp;(0633) 31555</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Email&nbsp;</td>\r\n			<td>:</td>\r\n			<td colspan=\"4\">&nbsp;diskominfo@humbanghasundutankab.go.id</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<hr />\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><iframe frameborder=\"0\" height=\"450\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3986.6996793830294!2d98.76777421426368!3d2.265541658581495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x302e3cb7ff0bcc73%3A0x2c1a5b16f34531dc!2sDinas+Komunikasi+dan+Informatika+Kab.+Humbang+Hasundutan!5e0!3m2!1sen!2sid!4v1561433370666!5m2!1sen!2sid\" style=\"border:0\" width=\"600\"></iframe></p>\r\n','2020-12-31',9,NULL,0,NULL,'admin','2019-06-26 08:27:46','admin','2019-06-26 08:27:46'),
	(5,1,'2020-07-25','Lorem Ipsum','lorem-ipsum','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\r\n\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</p>\r\n','2020-12-31',2,NULL,0,NULL,'admin','2020-07-25 20:04:53','admin','2020-07-25 20:04:53');

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Pengajar'),
	(3,'Pelajar');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','SISTEM E-ELEARNING'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC',''),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','-'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','-'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader-128x/Ellipsis-3s-200px.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tforum
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tforum`;

CREATE TABLE `tforum` (
  `Kd_Forum` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_TahunAjaran` bigint(10) NOT NULL,
  `Kd_Kelas` bigint(10) NOT NULL,
  `Kd_MataPelajaran` bigint(10) NOT NULL,
  `Kd_Sesi` bigint(10) NOT NULL,
  `Nm_Hari` varchar(10) NOT NULL,
  `Nm_Forum` varchar(50) NOT NULL,
  `Nm_FileLocation` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Forum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tforumdiskusi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tforumdiskusi`;

CREATE TABLE `tforumdiskusi` (
  `Kd_Forum` bigint(10) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Nm_FileLocation` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Forum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tkelasjadwal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tkelasjadwal`;

CREATE TABLE `tkelasjadwal` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_TahunAjaran` bigint(10) NOT NULL,
  `Kd_Kelas` bigint(10) NOT NULL,
  `Kd_MataPelajaran` bigint(10) NOT NULL,
  `Kd_Sesi` bigint(10) NOT NULL,
  `Nm_Hari` varchar(10) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tkelasjadwal` WRITE;
/*!40000 ALTER TABLE `tkelasjadwal` DISABLE KEYS */;

INSERT INTO `tkelasjadwal` (`Uniq`, `Kd_TahunAjaran`, `Kd_Kelas`, `Kd_MataPelajaran`, `Kd_Sesi`, `Nm_Hari`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,2,1,1,1,'1. SENIN','admin','2020-07-19 06:55:58',NULL,NULL),
	(2,2,1,1,2,'1. SENIN','admin','2020-07-19 06:56:03',NULL,NULL),
	(3,2,1,2,3,'1. SENIN','admin','2020-07-19 06:56:11',NULL,NULL),
	(4,2,1,2,4,'1. SENIN','admin','2020-07-19 06:56:15',NULL,NULL),
	(5,2,1,7,5,'1. SENIN','admin','2020-07-19 06:56:22',NULL,NULL),
	(6,2,1,7,6,'1. SENIN','admin','2020-07-19 06:56:28',NULL,NULL),
	(7,2,2,1,3,'1. SENIN','admin','2020-07-25 18:22:16',NULL,NULL),
	(8,2,2,1,4,'1. SENIN','admin','2020-07-25 18:22:23',NULL,NULL),
	(9,2,2,1,1,'2. SELASA','admin','2020-07-25 18:22:28',NULL,NULL),
	(10,2,2,1,2,'2. SELASA','admin','2020-07-25 18:22:35',NULL,NULL);

/*!40000 ALTER TABLE `tkelasjadwal` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tkelasmatapelajaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tkelasmatapelajaran`;

CREATE TABLE `tkelasmatapelajaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_TahunAjaran` bigint(10) NOT NULL,
  `Kd_Kelas` bigint(10) NOT NULL,
  `Kd_MataPelajaran` bigint(10) NOT NULL,
  `Kd_Pengajar` bigint(10) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tkelasmatapelajaran` WRITE;
/*!40000 ALTER TABLE `tkelasmatapelajaran` DISABLE KEYS */;

INSERT INTO `tkelasmatapelajaran` (`Uniq`, `Kd_TahunAjaran`, `Kd_Kelas`, `Kd_MataPelajaran`, `Kd_Pengajar`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,2,1,1,4,'admin','2020-07-19 06:55:18',NULL,NULL),
	(2,2,1,2,2,'admin','2020-07-19 06:55:24',NULL,NULL),
	(3,2,1,7,1,'admin','2020-07-19 06:55:29',NULL,NULL),
	(4,2,2,1,4,'admin','2020-07-25 14:37:44',NULL,NULL),
	(5,2,1,6,1,'admin','2022-11-07 07:23:10',NULL,NULL);

/*!40000 ALTER TABLE `tkelasmatapelajaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tkelaspelajar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tkelaspelajar`;

CREATE TABLE `tkelaspelajar` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_TahunAjaran` bigint(10) NOT NULL,
  `Kd_Kelas` bigint(10) NOT NULL,
  `Kd_Pelajar` bigint(10) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tkelaspelajar` WRITE;
/*!40000 ALTER TABLE `tkelaspelajar` DISABLE KEYS */;

INSERT INTO `tkelaspelajar` (`Uniq`, `Kd_TahunAjaran`, `Kd_Kelas`, `Kd_Pelajar`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,2,1,1,'admin','2020-07-19 06:55:39',NULL,NULL),
	(2,2,1,2,'admin','2020-07-19 06:55:41',NULL,NULL),
	(3,2,1,3,'admin','2020-07-19 06:55:45',NULL,NULL);

/*!40000 ALTER TABLE `tkelaspelajar` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tmateri
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tmateri`;

CREATE TABLE `tmateri` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `Kd_TahunAjaran` bigint(10) NOT NULL,
  `Kd_Kelas` bigint(10) NOT NULL,
  `Kd_MataPelajaran` bigint(10) NOT NULL,
  `Nm_Judul` varchar(200) NOT NULL DEFAULT '',
  `Nm_Keterangan` text,
  `Nm_File` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tnilai
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tnilai`;

CREATE TABLE `tnilai` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `Kd_TahunAjaran` bigint(10) NOT NULL,
  `Kd_Kelas` bigint(10) NOT NULL,
  `Kd_Pelajar` bigint(10) NOT NULL,
  `Kd_MataPelajaran` bigint(10) NOT NULL,
  `Nm_Komponen` varchar(200) NOT NULL DEFAULT '',
  `Bobot` double NOT NULL,
  `Nilai` double NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tnilai` WRITE;
/*!40000 ALTER TABLE `tnilai` DISABLE KEYS */;

INSERT INTO `tnilai` (`Uniq`, `Kd_TahunAjaran`, `Kd_Kelas`, `Kd_Pelajar`, `Kd_MataPelajaran`, `Nm_Komponen`, `Bobot`, `Nilai`, `CreatedOn`, `CreatedBy`, `UpdatedOn`, `UpdatedBy`)
VALUES
	(1,2,1,1,2,'1. KEHADIRAN',10,80,'2020-07-19 06:57:18','admin',NULL,NULL),
	(2,2,1,1,2,'2. TUGAS',10,80,'2020-07-19 06:57:23','admin',NULL,NULL),
	(3,2,1,1,2,'3. ULANGAN',15,82,'2020-07-19 06:57:30','admin',NULL,NULL),
	(4,2,1,1,2,'4. UTS',30,85,'2020-07-19 06:57:38','admin',NULL,NULL),
	(5,2,1,1,2,'5. UAS',35,79.5,'2020-07-19 06:57:44','admin',NULL,NULL),
	(6,2,1,2,2,'1. KEHADIRAN',10,85,'2020-07-19 06:57:50','admin',NULL,NULL),
	(7,2,1,2,2,'2. TUGAS',10,85,'2020-07-19 06:57:54','admin',NULL,NULL),
	(8,2,1,2,2,'3. ULANGAN',15,90,'2020-07-19 06:57:59','admin',NULL,NULL),
	(9,2,1,2,2,'4. UTS',30,87.5,'2020-07-19 06:58:04','admin',NULL,NULL),
	(10,2,1,2,2,'5. UAS',35,92,'2020-07-19 06:58:11','admin',NULL,NULL),
	(11,2,1,3,2,'1. KEHADIRAN',10,100,'2020-07-19 06:58:18','admin',NULL,NULL),
	(12,2,1,3,2,'2. TUGAS',10,97,'2020-07-19 06:58:24','admin',NULL,NULL),
	(13,2,1,3,2,'3. ULANGAN',15,88.75,'2020-07-19 06:58:30','admin',NULL,NULL),
	(14,2,1,3,2,'4. UTS',30,82.5,'2020-07-19 06:58:39','admin',NULL,NULL),
	(15,2,1,3,2,'5. UAS',35,97.75,'2020-07-19 06:58:48','admin',NULL,NULL),
	(16,2,1,1,1,'1. KEHADIRAN',10,80,'2020-07-25 19:30:14','444444','2020-07-25 19:30:28','444444'),
	(17,2,1,1,1,'2. TUGAS',10,78,'2020-07-25 19:30:49','444444',NULL,NULL),
	(18,2,1,1,1,'3. ULANGAN',15,82,'2020-07-25 19:30:56','444444',NULL,NULL),
	(19,2,1,1,1,'4. UTS',30,70,'2020-07-25 19:31:06','444444',NULL,NULL),
	(20,2,1,1,1,'5. UAS',35,82,'2020-07-25 19:31:11','444444',NULL,NULL);

/*!40000 ALTER TABLE `tnilai` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userinformation`;

CREATE TABLE `userinformation` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Nm_Email` varchar(50) NOT NULL DEFAULT '',
  `Nm_FullName` varchar(50) DEFAULT NULL,
  `Nm_IdentityNo` varchar(50) DEFAULT NULL,
  `DATE_Birth` date DEFAULT NULL,
  `Nm_Gender` varchar(50) DEFAULT NULL,
  `Nm_Address` text,
  `Nm_PhoneNo` varchar(50) DEFAULT NULL,
  `Nm_ImageLocation` varchar(250) DEFAULT NULL,
  `Nm_About` text,
  `DATE_Registered` date DEFAULT NULL,
  PRIMARY KEY (`UserName`),
  UNIQUE KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `userinformation` WRITE;
/*!40000 ALTER TABLE `userinformation` DISABLE KEYS */;

INSERT INTO `userinformation` (`Uniq`, `UserName`, `Nm_Email`, `Nm_FullName`, `Nm_IdentityNo`, `DATE_Birth`, `Nm_Gender`, `Nm_Address`, `Nm_PhoneNo`, `Nm_ImageLocation`, `Nm_About`, `DATE_Registered`)
VALUES
	(5,'12121111','sims.anyageraldine@sims.co.id',NULL,NULL,NULL,NULL,NULL,'081211221212',NULL,NULL,'2020-07-25'),
	(4,'333333','sims.drsoetomo@sims.co.id',NULL,NULL,NULL,NULL,NULL,'081211221212',NULL,NULL,'2020-07-25'),
	(6,'444444','sims.irhabibie@sims.co.id',NULL,NULL,NULL,NULL,NULL,'-',NULL,NULL,'2020-07-25'),
	(1,'admin','yoelrolas@gmail.com','Administrator',NULL,NULL,NULL,NULL,'085359867032','admin.png',NULL,'2018-08-17');

/*!40000 ALTER TABLE `userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('12121111','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-07-25 19:32:05','::1'),
	('333333','e10adc3949ba59abbe56e057f20f883e',2,0,'2020-07-25 13:30:28','::1'),
	('444444','e10adc3949ba59abbe56e057f20f883e',2,0,'2022-11-07 07:24:52','::1'),
	('admin','e10adc3949ba59abbe56e057f20f883e',1,0,'2022-11-07 08:20:19','::1');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
