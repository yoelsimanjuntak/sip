# SIP - Sistem Informasi Pendidikan

**Fitur Utama**
*  Manajemen Sekolah
*  Kurikulum
*  Akreditasi
*  Forum Belajar
*  Absensi
*  Penilaian
*  Alumni Tracer
*  Whistleblowing
*  Data Analytic

**Spesifikasi**
*  PHP 5.3
*  MariaDB 10.1
*  [CodeIgniter](https://codeigniter.com/)
*  [AdminLTE 3.0](https://adminlte.io/themes/v3/)
*  [Partopi Tao](https://gitlab.com/yoelsimanjuntak) Web Framework