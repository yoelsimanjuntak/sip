<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=!empty($title) ? SITENAME.' | '.$title : SITENAME?></title>

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/css/style.css" />
    <link href="<?=base_url()?>assets/themes/inspinia/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/themes/inspinia/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/themes/inspinia/css/plugins/bootstrapSocial/bootstrap-social.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/css/plugins/dataTables/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/css/plugins/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/css/plugins/slick/slick-theme.css">
    <style>
        .timeline-item .content {
            min-height: 70px !important;
        }
    </style>
    <style>
        .google-maps {
            position: relative;
            padding-bottom: 75%;
            height: 0;
            overflow: hidden;
        }
        .google-maps iframe {
            position: absolute;
            top: 0;
            left: 0;
            width: 100% !important;
            height: 100% !important;
        }
    </style>
</head>
<body id="page-top" class="landing-page top-navigation pace-done">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg" style="padding: 0px 0px !important;">
        <div class="navbar-wrapper">
            <nav class="navbar navbar-default navbar-fixed-top navbar-expand-md" role="navigation">
                <div class="container">
                    <a class="navbar-brand" href="<?=site_url()?>">
                        <?=SITENAME?>
                    </a>
                    <div class="navbar-header page-scroll">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse justify-content-end" id="navbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="nav-link page-scroll" href="<?=site_url()?>">Beranda</a></li>
                            <li><a class="nav-link page-scroll" href="<?=site_url('post/archive')?>">Berita</a></li>
                            <li><a class="nav-link page-scroll" href="<?=site_url('post/gallery')?>">Galeri</a></li>
                            <li><a class="nav-link page-scroll" href="<?=site_url('post/event')?>">Kegiatan</a></li>
                            <li><a class="nav-link page-scroll" href="#">Hubungi Kami</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>


