<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=!empty($title) ? SITENAME.' | '.$title : SITENAME?></title>

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/css/style.css" />
    <link href="<?=base_url()?>assets/themes/inspinia/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/themes/inspinia/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/themes/inspinia/css/plugins/bootstrapSocial/bootstrap-social.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/css/plugins/dataTables/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/css/plugins/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/inspinia/css/plugins/slick/slick-theme.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/inspinia/css/plugins/blueimp/css/blueimp-gallery.min.css">
    <link rel="icon" type="image/png" href="<?=base_url()?>assets/media/image/favicon.png">
    <style>
        .timeline-item .content {
            min-height: 70px !important;
        }
    </style>
    <style>
        .google-maps {
            position: relative;
            padding-bottom: 75%;
            height: 0;
            overflow: hidden;
        }
        .google-maps iframe {
            position: absolute;
            top: 0;
            left: 0;
            width: 100% !important;
            height: 100% !important;
        }
        .navy-line {
            width: 60px;
            height: 1px;
            margin: 60px auto 0;
            margin-top: 40px;
            margin-right: auto;
            margin-bottom: 0px;
            margin-left: auto;
            border-bottom: 2px solid #1ab394;
        }

        .grid .ibox {
            margin-bottom: 0;
        }

        .grid-item {
            margin-bottom: 25px;
            width: 300px;
        }

        .slick-news .ibox-content {
            margin: 0 10px;
        }
    </style>
</head>
<body class="top-navigation">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-expand-lg navbar-static-top" role="navigation">
                <!--<div class="navbar-header">-->
                <!--<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">-->
                <!--<i class="fa fa-reorder"></i>-->
                <!--</button>-->

                <a href="#" class="navbar-brand"><!--<img src="<?=base_url()?>assets/media/image/logo.png" />--> <?=SITENAME?></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-reorder"></i>
                </button>

                <!--</div>-->
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav mr-auto">
                        <li><a href="<?=site_url()?>">Beranda</a></li>
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">&nbsp;Profil</a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="<?=site_url('post/page/p101')?>">Kata Sambutan</a></li>
                                <li><a href="<?=site_url('post/view/maklumat-pelayanan')?>">Maklumat</a></li>
                                <li><a href="<?=site_url('post/page/p103')?>">Organisasi</a></li>
                                <li><a href="<?=site_url('post/page/p10')?>">SOP</a></li>
                            </ul>
                        </li>
                        <li><a href="<?=site_url('post/archive')?>">Berita</a></li>
                        <li><a href="<?=site_url('post/event')?>">Kegiatan</a></li>
                        <li><a href="<?=site_url('post/gallery')?>">Galeri</a></li>
                        <li><a href="<?=site_url('post/view/hubungi-kami')?>">Hubungi Kami</a></li>
                    </ul>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Kata kunci...">
                                <span class="input-group-append">
                                    <button type="button" class="btn btn-primary btn-outline"><i class="fa fa-search"></i> Cari</button>
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="wrapper wrapper-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 animated fadeInUp">