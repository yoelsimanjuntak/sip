<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 04/10/2018
 * Time: 09:45
 */
?>
<?php $this->load->view('frontend/header') ?>
<style>
    .product-box .product-desc .small {
        height: 30px !important;
    }
    .product-box .product-name {
        font-size: 14px;
    }
</style>
<p class="small font-italic">*) Klik untuk melihat detil anggota</p>
    <div class="ibox">
        <div class="ibox-content text-center">
            <a data-toggle="collapse" href="#kaban" class="" aria-expanded="true">
                <h3 class="m-b-xxs">Kepala Badan</h3>
            </a>
        </div>
    </div>
    <div class="row collapse" id="kaban">
        <div class="col-md-4" style="margin-left: auto; margin-right: auto">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/AMAS.jpg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Pembina Utama Muda</small>
                        <a href="#" class="product-name"> AMAS MUDA, SH</a>
                        <div class="small m-t-xs">
                            Kepala Badan Kesatuan Bangsa Politik dan Perlindungan Masyarakat
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-content text-center">
            <a data-toggle="collapse" href="#sekretariat" class="" aria-expanded="true">
                <h3 class="m-b-xxs">Sekretariat</h3>
                
            </a>
        </div>
    </div>
    <div class="row collapse" id="sekretariat">
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/2.ERWANDI.JPG"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Pembina TK. I</small>
                        <a href="#" class="product-name"> ERWANDI, SH</a>
                        <div class="small m-t-xs">
                            Sekretaris Badan Kesatuan Bangsa Politik dan Perlindungan Masyarakat
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/3.ALDEN.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata TK. I</small>
                        <a href="#" class="product-name"> Drs. ALDEN SIRAIT</a>
                        <div class="small m-t-xs">
                            Kepala Sub Bagian Program dan Perundang-undangan
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/4.ERNA.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata TK. I</small>
                        <a href="#" class="product-name"> ERNA, S.Pd</a>
                        <div class="small m-t-xs">
                            Kepala Sub Bagian Umum dan Kepegawaian
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/7.DEWI.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata TK. I</small>
                        <a href="#" class="product-name"> SRI DEWI ASTUTI, SE</a>
                        <div class="small m-t-xs">
                            Kepala Sub Bagian Keuangan
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/5.ISNIAR.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata Muda</small>
                        <a href="#" class="product-name"> ISNIAR HANDAYANI</a>
                        <div class="small m-t-xs">
                            Pengadministrasi Umum
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/ARIF.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata Muda</small>
                        <a href="#" class="product-name"> MUHAMMAD ARIF</a>
                        <div class="small m-t-xs">
                            Analisis Kebijakan Klarifikasi Barang
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/8.DINA.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata</small>
                        <a href="#" class="product-name"> HERDINA PURBA, SE</a>
                        <div class="small m-t-xs">
                            Analisis Rencana Perundangan
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/DIAN.jpg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata Muda</small>
                        <a href="#" class="product-name"> DIAN H NASUTION, A.Md</a>
                        <div class="small m-t-xs">
                            Bendahara
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/ADUL.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Pengatur Muda TK. I</small>
                        <a href="#" class="product-name"> ABDULLAH</a>
                        <div class="small m-t-xs">
                            Pengelola / Bendahara Barang
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/user.jpg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Pengatur</small>
                        <a href="#" class="product-name"> YOEL ROLAS S, A.Md</a>
                        <div class="small m-t-xs">
                            Pengelola Teknologi Informasi
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/user.jpg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata Muda</small>
                        <a href="#" class="product-name"> NOVI ANGGRAINI, SE</a>
                        <div class="small m-t-xs">
                            Analisis Laporan Keuangan
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/27.MEI RINA K.HASIBUAN,SE.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Staff</small>
                        <a href="#" class="product-name"> MEI RINA K. HASIBUAN</a>
                        <!--<div class="small m-t-xs">
                            Pengelola Teknologi Informasi
                        </div>-->
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/user.jpg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Staff</small>
                        <a href="#" class="product-name"> Satria Sagita</a>
                        <!--<div class="small m-t-xs">
                            Pengelola Teknologi Informasi
                        </div>-->
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/28.FARDIAN ISKANDAR LUBIS.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Staff</small>
                        <a href="#" class="product-name"> FARDIAN ISKANDAR LUBIS</a>
                        <!--<div class="small m-t-xs">
                            Pengelola Teknologi Informasi
                        </div>-->
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/31.UMA RUMAIYAH.JPG"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Staff</small>
                        <a href="#" class="product-name"> UMA RUMAIYAH</a>
                        <!--<div class="small m-t-xs">
                            Pengelola Teknologi Informasi
                        </div>-->
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-content text-center">
            <a data-toggle="collapse" href="#politik" class="" aria-expanded="true">
                <h3 class="m-b-xxs">Bidang Politik Dalam Negeri</h3>
                
            </a>
        </div>
    </div>
    <div class="row collapse" id="politik">
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/HARBAIN.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Pembina TK. I</small>
                        <a href="#" class="product-name"> HARBAIN, SE</a>
                        <div class="small m-t-xs">
                            Kepala Bidang Politik Dalam Negeri
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/PINTA ULI.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata TK. I</small>
                        <a href="#" class="product-name"> PINTA ULI, SH</a>
                        <div class="small m-t-xs">
                            Kepala Sub Bidang Pendidikan Politik dan Fasilitas Pemilu
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/13.ISWANDI.JPG"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata</small>
                        <a href="#" class="product-name"> ISWANDI SARAGIH, SH</a>
                        <div class="small m-t-xs">
                            Kepala Sub Bidang Kelembagaan Politik, Pemerintahan dan Parpol
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/BAHTIAR.jpg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Pengatur Muda TK. I</small>
                        <a href="#" class="product-name"> BAHTIAR SINAGA</a>
                        <div class="small m-t-xs">
                            Operator Komputer
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-content text-center">
            <a data-toggle="collapse" href="#wasbang" class="" aria-expanded="true">
                <h3 class="m-b-xxs">Bidang Idiologi dan Wawasan Kebangsaaan</h3>
                
            </a>
        </div>
    </div>
    <div class="row collapse" id="wasbang">
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/15.YUSDI.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata TK. I</small>
                        <a href="#" class="product-name"> M. YUSDI, SH</a>
                        <div class="small m-t-xs">
                            Kepala Bidang Idiologi dan Wawasan Kebangsaaan
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/16.M.CHOLID.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata TK. I</small>
                        <a href="#" class="product-name"> M. CHOLID RIDHA, SE</a>
                        <div class="small m-t-xs">
                            Kepala Sub Bidang Ketahanan Idiologi dan Wawasan Kebangsaan
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/17.MARTIN.JPG"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata</small>
                        <a href="#" class="product-name"> MARTHIN ALEXANDER, SH</a>
                        <div class="small m-t-xs">
                            Kepala Sub Bidang Bela Negara dan Linmas
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/18.ABD.HAYAT.JPG"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata</small>
                        <a href="#" class="product-name"> ABD. HAYAT SINAMBELA, S.Sos</a>
                        <div class="small m-t-xs">
                            Analisis Wawasan Kebangsaan
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-content text-center">
            <a data-toggle="collapse" href="#wasnas" class="" aria-expanded="true">
                <h3 class="m-b-xxs">Bidang Ketahanan Ekonomi dan Wawasan Nasional</h3>
                
            </a>
        </div>
    </div>
    <div class="row collapse" id="wasnas">
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/19.AGUSTINUS.JPG"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata TK. I</small>
                        <a href="#" class="product-name"> Drs. AGUSTINUS</a>
                        <div class="small m-t-xs">
                            Kepala Bidang Ketahanan Ekonomi dan Wawasan Nasional
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/20.ARISMAN.JPG"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata</small>
                        <a href="#" class="product-name"> ARISMAN</a>
                        <div class="small m-t-xs">
                            Kepala Sub Bidang Ketahanan Ekonomi
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/user.jpg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata Muda</small>
                        <a href="#" class="product-name"> RISNA FARIDA LUBIS, SE</a>
                        <div class="small m-t-xs">
                            Analisis Ketahanan Ekonomi
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/25.SIDI GUNAWAN.JPG"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Staff</small>
                        <a href="#" class="product-name"> SIDI GUNAWAN</a>
                        <!--<div class="small m-t-xs">
                            Pengelola Teknologi Informasi
                        </div>-->
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/26.DODI HERIZAL.jpeg"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Staff</small>
                        <a href="#" class="product-name"> DODI HERIZAL</a>
                        <!--<div class="small m-t-xs">
                            Pengelola Teknologi Informasi
                        </div>-->
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/29.FANNI ALVITANIA.JPG"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Staff</small>
                        <a href="#" class="product-name"> FANNI ALVITANIA</a>
                        <!--<div class="small m-t-xs">
                            Pengelola Teknologi Informasi
                        </div>-->
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-content text-center">
            <a data-toggle="collapse" href="#senbud" class="" aria-expanded="true">
                <h3 class="m-b-xxs">Bidang Ketahanan Seni, Budaya, Agama / Kemasyarakatan</h3>
                
            </a>
        </div>
    </div>
    <div class="row collapse" id="senbud">
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/22.FARIDA.JPG"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Pembina</small>
                        <a href="#" class="product-name"> Dra. Hj. FARIDA HANUM</a>
                        <div class="small m-t-xs">
                            Kepala Bidang Ketahanan Seni, Budaya, Agama / Kemasyarakatan
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/23.NELSON.JPG"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata TK. I</small>
                        <a href="#" class="product-name"> NELSON SITORUS, SH</a>
                        <div class="small m-t-xs">
                            Kepala Sub Bidang Pembauran, Budaya, dan Organisasi Kemasyarakatan
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-content product-box">
                    <div class="product-imitation" style="background-image: url('<?=MY_IMAGEURL."profiles/24.ILHAM.JPG"?>');background-size: cover;background-repeat-x: no-repeat;background-position-x: center; padding: 120px 0px">

                    </div>
                    <div class="product-desc">
                        <small class="text-muted">Penata TK. I</small>
                        <a href="#" class="product-name"> MOCH. ILHAM, SH, M.Si</a>
                        <div class="small m-t-xs">
                            Kepala Sub Bidang Ketahanan Seni, Budaya, Agama dan Kemasyarakatan
                        </div>
                        <div class="m-t text-center">
                            <a href="" class="btn btn-outline btn-success dim"><i class="fa fa-facebook"></i></a>
                            <a href="" class="btn btn-outline btn-danger dim"><i class="fa fa-instagram"></i></a>
                            <a href="" class="btn btn-outline btn-info dim"><i class="fa fa-twitter"></i></a>
                            <a href="" class="btn btn-outline btn-primary dim"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('frontend/footer') ?>