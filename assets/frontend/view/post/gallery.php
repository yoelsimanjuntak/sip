<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 04/10/2018
 * Time: 11:48
 */
?>
<?php $this->load->view('frontend/header') ?>
    <div class="row animated fadeInUp">
        <?php
        if(count($data) > 0) {
            foreach($data as $n) {
                $file_ = $this->db->where(COL_POSTID, $n[COL_POSTID])->get(TBL_POSTIMAGES)->row_array();
                ?>
                <div class="col-lg-6">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5><?=$n[COL_POSTTITLE]?></h5>
                        </div>
                        <div>
                            <div class="ibox-content no-padding border-left-right" style="height: 30vh; background-image: url('<?=MY_UPLOADURL.$file_[COL_FILENAME]?>'); background-size: cover">
                                <!--<img alt="<?=$n[COL_POSTTITLE]?>" class="img-fluid" src="<?=MY_UPLOADURL.$file_[COL_FILENAME]?>">-->
                            </div>
                            <div class="ibox-content profile-content" style="height: 180px !important;">
                                <h4><strong><?=$n[COL_NAME]?></strong></h4>
                                <p><i class="fa fa-clock-o"></i> <?=date("D, d M Y", strtotime($n[COL_CREATEDON]))?></p>
                                <?php
                                $strippedcontent = strip_tags($n[COL_POSTCONTENT]);
                                ?>
                                <p style="text-align: justify">
                                    <?=strlen($strippedcontent) > 200 ? substr($strippedcontent, 0, 200) . "..." : $strippedcontent ?>
                                </p>
                            </div>
                        </div>
                        <div class="ibox-footer">
                            <div class="col-md-12">
                                <a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>" class="btn btn-primary btn-sm btn-block btn-flat">Lihat selengkapnya <i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        } else {
            ?>
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <p class="font-italic">Tidak ada data untuk ditampilkan.</p>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
<?php $this->load->view('frontend/footer') ?>