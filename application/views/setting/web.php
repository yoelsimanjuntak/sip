<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 20/07/2019
 * Time: 09:35
 */
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-primary">
                        <div class="card-body">
                            <?php if(validation_errors()){ ?>
                                <div class="alert alert-danger">
                                    <i class="fa fa-ban"></i> PESAN ERROR :
                                    <ul>
                                        <?= validation_errors() ?>
                                    </ul>

                                </div>
                            <?php } ?>

                            <?php if(!empty($errormess)){ ?>
                                <div class="alert alert-danger">
                                    <i class="fa fa-ban"></i> PESAN ERROR :
                                    <?= $errormess ?>
                                </div>
                            <?php } ?>

                            <?php  if($this->input->get('success')){ ?>
                                <div class="form-group alert alert-success alert-dismissible">
                                    <i class="fa fa-check"></i>
                                    Berhasil.
                                </div>
                            <?php } ?>

                            <?php  if($this->input->get('error')){ ?>
                                <div class="form-group alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    Gagal mengupdate data, silahkan coba kembali
                                </div>
                            <?php } ?>

                            <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label  class="control-label col-sm-3">Name</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="<?=SETTING_WEB_NAME?>" value="<?=!empty($data[SETTING_WEB_NAME]) ? $data[SETTING_WEB_NAME] : $this->setting_web_name?>" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="control-label col-sm-3">Description</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="<?=SETTING_WEB_DESC?>" value="<?=!empty($data[SETTING_WEB_DESC]) ? $data[SETTING_WEB_DESC] : $this->setting_web_desc?>" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="control-label col-sm-3">Version</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="<?=SETTING_WEB_VERSION?>" value="<?=!empty($data[SETTING_WEB_VERSION]) ? $data[SETTING_WEB_VERSION] : $this->setting_web_version?>" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="control-label col-sm-3">Logo</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="<?=SETTING_WEB_LOGO?>" value="<?=!empty($data[SETTING_WEB_LOGO]) ? $data[SETTING_WEB_LOGO] : $this->setting_web_logo?>" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="control-label col-sm-3">Preloader</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="<?=SETTING_WEB_PRELOADER?>" value="<?=!empty($data[SETTING_WEB_PRELOADER]) ? $data[SETTING_WEB_PRELOADER] : $this->setting_web_preloader?>" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="control-label col-sm-3">Skin Class</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="<?=SETTING_WEB_SKIN_CLASS?>" value="<?=!empty($data[SETTING_WEB_SKIN_CLASS]) ? $data[SETTING_WEB_SKIN_CLASS] : $this->setting_web_skin_class?>" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="control-label col-sm-3">DISQUS URL</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="<?=SETTING_WEB_DISQUS_URL?>" value="<?=!empty($data[SETTING_WEB_DISQUS_URL]) ? $data[SETTING_WEB_DISQUS_URL] : $this->setting_web_disqus_url?>" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="control-label col-sm-3">Footer Link API</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="<?=SETTING_WEB_API_FOOTERLINK?>" value="<?=!empty($data[SETTING_WEB_API_FOOTERLINK]) ? $data[SETTING_WEB_API_FOOTERLINK] : $this->setting_web_api_footerlink?>" required />
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr />
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <button type="submit" class="btn btn-primary btn-flat pull-right">Simpan</button>
                                </div>
                            </div>
                            <?=form_close()?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
<?php $this->load->view('footer') ?>