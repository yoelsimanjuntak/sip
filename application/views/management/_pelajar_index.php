<table class="table table-bordered">
  <tbody>
    <tr>
      <th>Nama</th>
      <th>No. Induk</th>
      <th class="text-center" style="width: 10px; white-space: nowrap;">#</th>
    </tr>
    <?php
    foreach ($rpelajar as $m) {
      ?>
      <tr>
        <td><?=$m[COL_NM_PELAJAR]?></td>
        <td><?=$m[COL_NM_NOMORINDUKSISWA]?></td>
        <td class="text-center" style="width: 10px; white-space: nowrap;">
          <?=anchor(site_url('management/pelajar-delete/'.$m[COL_UNIQ]), '<i class="fa fa-times"></i>&nbsp;HAPUS', array('class'=>'btn btn-xs btn-danger btn-delete'))?>
          </td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>
