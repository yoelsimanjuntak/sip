<?php $this->load->view('layouts/backend-header'); ?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <?=form_open(site_url('management/jadwal'), array('id'=>'form-filter', 'method'=>'get'))?>
            <div class="form-group row mb-0">
              <label class="control-label col-sm-2">Tahun Ajaran</label>
              <div class="col-sm-2">
                <select name="<?=COL_KD_TAHUNAJARAN?>" class="form-control form-sm">
                  <?=GetCombobox("select * from mtahunajaran order by Nm_TahunAjaran", COL_KD_TAHUNAJARAN, COL_NM_TAHUNAJARAN, (!empty($kdTA)?$kdTA:null), true, false, '-- Tahun Ajaran --')?>
                </select>
              </div>
              <label class="control-label col-sm-2">Kelas</label>
              <div class="col-sm-2">
                <select name="<?=COL_KD_KELAS?>" class="form-control form-sm">
                  <?=GetCombobox("select * from mkelas order by Nm_Kelas", COL_KD_KELAS, COL_NM_KELAS, (!empty($kdKelas)?$kdKelas:null), true, false, '-- Kelas --')?>
                </select>
              </div>
            </div>
            <?=form_close()?>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <th>Hari</th>
                      <th>Sesi</th>
                      <th>Mata Pelajaran</th>
                      <th class="text-center" style="width: 10px; white-space: nowrap;">
                        <a href="<?=site_url('management/jadwal-add/'.$kdTA.'/'.$kdKelas)?>" class="btn btn-xs btn-outline-success btn-popup"><i class="fa fa-plus"></i>&nbsp;TAMBAH</a>
                      </th>
                    </tr>
                    <?php
                    foreach ($rjadwal as $jad) {
                      ?>
                      <tr>
                        <td><?=$jad[COL_NM_HARI]?></td>
                        <td><?=$jad[COL_NM_SESI].' - '.$jad[COL_JAM_FROM].' s.d '.$jad[COL_JAM_TO]?></td>
                        <td><?=$jad[COL_NM_MATAPELAJARAN]?></td>
                        <td class="text-center">
                          <a href="<?=site_url('management/jadwal-delete/'.$jad[COL_UNIQ])?>" class="btn btn-xs btn-outline-danger btn-delete"><i class="fa fa-times"></i>&nbsp;HAPUS</a>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-popup" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Jadwal</h4>
      </div>
      <div class="modal-body">
        <form id="form-jadwal" method="post" action="#">
          <div class="form-group">
            <label>Hari</label>
            <select name="<?=COL_NM_HARI?>" class="form-control form-sm" style="width: 100%">
              <option value="1. SENIN">SENIN</option>
              <option value="2. SELASA">SELASA</option>
              <option value="3. RABU">RABU</option>
              <option value="4. KAMIS">KAMIS</option>
              <option value="5. JUMAT">JUMAT</option>
            </select>
          </div>
          <div class="form-group">
            <label>Sesi</label>
            <select name="<?=COL_KD_SESI?>" class="form-control form-sm" style="width: 100%">
              <?php
              foreach ($rsesi as $ses) {
                ?>
                <option value="<?=$ses[COL_KD_SESI]?>"><?=$ses[COL_NM_SESI].' - '.$ses[COL_JAM_FROM].' s.d '.$ses[COL_JAM_TO]?></option>
                <?php
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label>Mata Pelajaran</label>
            <select name="<?=COL_KD_MATAPELAJARAN?>" class="form-control form-sm" style="width: 100%">
              <?=GetCombobox("select tkelasmatapelajaran.*, mmatapelajaran.Nm_MataPelajaran from tkelasmatapelajaran left join mmatapelajaran on mmatapelajaran.Kd_MataPelajaran = tkelasmatapelajaran.Kd_MataPelajaran where Kd_TahunAjaran = $kdTA and Kd_Kelas = $kdKelas order by Nm_MataPelajaran", COL_KD_MATAPELAJARAN, COL_NM_MATAPELAJARAN)?>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;CLOSE</button>
        <button type="button" class="btn btn-primary btn-sm btn-ok"><i class="fa fa-check"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/_js'); ?>
<script>
$(document).ready(function() {
  var modalpop = $('#modal-popup');

  modalpop.on('hidden.bs.modal', function (event) {
    location.reload();
  });

  $('select, input', $('#form-filter')).change(function() {
    $('#form-filter').submit();
  });

  $('.btn-popup, .btn-popup-edit').click(function(){
    var kdTA = $('[name=Kd_TahunAjaran]').val();
    var kdKelas = $('[name=Kd_Kelas]').val();

    if(!kdTA) {
      alert('Silakan pilih Tahun Ajaran!');
      return false;
    }
    if(!kdKelas) {
      alert('Silakan pilih Kelas!');
      return false;
    }

    var a = $(this);
    var hari = $(this).data('hari');
    var sesi = $(this).data('sesi');
    var mapel = $(this).data('mapel');

    if(hari) $('[name=<?=COL_NM_HARI?>]', modalpop).val(hari).trigger('change');
    if(sesi) $('[name=<?=COL_KD_SESI?>]', modalpop).val(sesi).trigger('change');
    if(mapel) $('[name=<?=COL_KD_MATAPELAJARAN?>]', modalpop).val(mapel).trigger('change');
    modalpop.modal("show");
    $(".btn-ok", modalpop).unbind('click').click(function() {
      var dis = $(this);
      dis.attr("disabled", true);
      $('#form-jadwal', modalpop).ajaxSubmit({
        dataType: 'json',
        url : a.attr('href'),
        success : function(data){
          if(data.error==0){
            modalpop.modal('hide');
          } else{
            toastr.error(data.error);
          }
        },
        complete: function() {
          dis.attr("disabled", false);
          //toastr.error("Server Error.");
        }
      });
    });
    return false;
  });

  $('.btn-delete').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.post(url, function(data) {
        if(data.error == 0) {
          location.reload();
        } else {
          toastr.error(data.error);
        }
      }, 'json');
    }
    return false;
  });
});
</script>
<?php $this->load->view('layouts/backend-footer'); ?>
