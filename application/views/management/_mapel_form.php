<?=form_open(site_url('management/matapelajaran-add/'.$kdTA.'/'.$kdKelas), array('id'=>'form-mapel'))?>
<div class="form-group row">
  <div class="col-sm-12">
    <div class="input-group" style="width: 100%">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-book"></i></span>
      </div>
      <select name="<?=COL_KD_MATAPELAJARAN?>" class="form-control form-sm">
        <?=GetCombobox("select * from mmatapelajaran order by Nm_MataPelajaran", COL_KD_MATAPELAJARAN, COL_NM_MATAPELAJARAN)?>
      </select>
      <div class="input-group-append">
        <span class="input-group-text"><i class="fa fa-user"></i></span>
      </div>
      <select name="<?=COL_KD_PENGAJAR?>" class="form-control form-sm">
        <?=GetCombobox("select * from mpengajar order by Nm_Pengajar", COL_KD_PENGAJAR, array(COL_NM_PENGAJAR, COL_NM_NOMORINDUKPEGAWAI))?>
      </select>
      <span class="input-group-append">
        <button type="submit" class="btn btn-outline-success"><i class="fa fa-plus"></i>&nbsp;TAMBAH</button>
      </span>
    </div>  
  </div>
</div>
<?=form_close()?>
<div class="row">
  <div id="div-mapel" class="col-sm-12">
    
  </div>
</div>

<script>
  function loadMapel() {
    $('#div-mapel').empty();
    $('#div-mapel').load('<?=site_url("management/matapelajaran-partial/".$kdTA."/".$kdKelas)?>', function(){
      $('.btn-delete', $('#div-mapel')).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.post(url, function(data) {
            if(data.error == 0) {
              loadMapel();
            } else {
              toastr.error(data.error);
            }
          }, 'json');
        }
        return false;
      });
    });
  }
  $(document).ready(function() {
    loadMapel();
    $('select', $('#form-mapel')).not('.no-select2').select2({ width: 'resolve', theme: 'bootstrap4' });
    $('#form-mapel').submit(function() {
      $(this).ajaxSubmit({
        dataType: 'json',
        type : 'post',
          success: function(data){
            if(data.error == 0) {
              loadMapel();
            } else {
              toastr.error(data.error);
            }
          },
          error: function(a,b,c){
            toastr.error('Response Error');
          }
      });
      return false;
    });
  });
</script>
