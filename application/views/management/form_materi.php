<?php $this->load->view('layouts/backend-header'); ?>
<?php
$ruser = GetLoggedUser();
$condKelas = "";
$condMapel = "where Kd_Kelas = $kdKelas";
if($ruser[COL_ROLEID] == ROLEGURU) {
  $mpengajar = $this->db
  ->where(COL_NM_NOMORINDUKPEGAWAI, $ruser[COL_USERNAME])
  ->get(TBL_MPENGAJAR)
  ->row_array();

  if(!empty($kdTA) && !empty($mpengajar)) {
    $arrkelas_ = array();
    $arrkelas = $this->db
    ->select(COL_KD_KELAS)
    ->where(array(
      COL_KD_TAHUNAJARAN=>$kdTA,
      COL_KD_PENGAJAR=>$mpengajar[COL_KD_PENGAJAR]
    ))
    ->group_by(COL_KD_KELAS)
    ->get(TBL_TKELASMATAPELAJARAN)
    ->result_array();
    foreach($arrkelas as $k) {
      $arrkelas_[] = $k[COL_KD_KELAS];
    }
    $arrkelas_ = implode("','", $arrkelas_);
    $condKelas = "where Kd_Kelas in ('$arrkelas_')";

    $arrmapel_ = array();
    $arrmapel = $this->db
    ->select(COL_KD_MATAPELAJARAN)
    ->where(array(
      COL_KD_TAHUNAJARAN=>$kdTA,
      COL_KD_PENGAJAR=>$mpengajar[COL_KD_PENGAJAR]
    ))
    ->group_by(COL_KD_MATAPELAJARAN)
    ->get(TBL_TKELASMATAPELAJARAN)
    ->result_array();
    foreach($arrmapel as $k) {
      $arrmapel_[] = $k[COL_KD_MATAPELAJARAN];
    }
    $arrmapel_ = implode("','", $arrmapel_);
    $condMapel = "where mmatapelajaran.Kd_MataPelajaran in ('$arrmapel_')";
  } else {
    $condKelas = "where 1 != 1";
  }
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <?=form_open(site_url('management/materi'), array('id'=>'form-filter', 'method'=>'get'))?>
            <div class="form-group row mb-0">
              <div class="col-sm-4">
                <label class="control-label">Tahun Ajaran</label>
                <select name="<?=COL_KD_TAHUNAJARAN?>" class="form-control form-sm" <?=$ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGURU ? 'disabled':''?>>
                  <?=GetCombobox("select * from mtahunajaran order by Nm_TahunAjaran", COL_KD_TAHUNAJARAN, COL_NM_TAHUNAJARAN, (!empty($kdTA)?$kdTA:null), true, false, '-- Tahun Ajaran --')?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="control-label">Kelas</label>
                <select name="<?=COL_KD_KELAS?>" class="form-control form-sm" <?=$ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGURU ? 'disabled':''?>>
                  <?=GetCombobox("select * from mkelas $condKelas order by Nm_Kelas", COL_KD_KELAS, COL_NM_KELAS, (!empty($kdKelas)?$kdKelas:null), true, false, '-- Kelas --')?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="control-label">Mata Pelajaran</label>
                <select name="<?=COL_KD_MATAPELAJARAN?>" class="form-control form-sm">
                  <?=GetCombobox("select distinct mmatapelajaran.Nm_MataPelajaran, mmatapelajaran.Kd_MataPelajaran from tkelasmatapelajaran left join mmatapelajaran on mmatapelajaran.Kd_MataPelajaran = tkelasmatapelajaran.Kd_MataPelajaran $condMapel order by Nm_MataPelajaran", COL_KD_MATAPELAJARAN, COL_NM_MATAPELAJARAN, (!empty($kdMapel)?$kdMapel:null), true, false, '-- Mata Pelajaran --')?>
                </select>
              </div>
            </div>
            <?=form_close()?>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <th style="white-space: nowrap; width: 120px;">Tanggal</th>
                        <th style="white-space: nowrap;">Judul</th>
                        <th>Keterangan</th>
                        <th class="text-center" style="white-space: nowrap; width: 10px;">
                          <?php
                          if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEGURU) {
                            ?>
                            <a href="<?=site_url('management/materi-add/'.$kdTA.'/'.$kdKelas.'/'.$kdMapel)?>" class="btn btn-xs btn-block btn-outline-success btn-tambah-materi"><i class="fa fa-plus"></i>&nbsp;TAMBAH</a>
                            <?php
                          }
                          ?>
                        </th>
                      </tr>
                      <?php
                      foreach ($rmateri as $p) {
                        ?>
                        <tr>
                          <td style="white-space: nowrap; width: 120px;"><?=date('Y-m-d H:i', strtotime($p[COL_CREATEDON]))?></td>
                          <td><?=$p[COL_NM_JUDUL]?></td>
                          <td><?=$p[COL_NM_KETERANGAN]?></td>
                          <td class="text-center" style="white-space: nowrap; width: 10px;">
                            <a href="<?=MY_UPLOADURL.$p[COL_NM_FILE]?>" class="btn btn-xs btn-outline-primary" target="_blank"><i class="fa fa-download"></i>&nbsp;UNDUH</a>
                            <?php
                            if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEGURU) {
                              ?>
                              <a href="<?=site_url('management/materi-delete/'.$p[COL_UNIQ])?>" class="btn btn-xs btn-outline-danger btn-delete-materi"><i class="fa fa-times"></i>&nbsp;HAPUS</a>
                              <?php
                            }
                            ?>
                          </td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-materi" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title font-weight-light">Materi</h6>
      </div>
      <div class="modal-body">
         <form id="form-materi" method="post" action="#" enctype="multipart/form-data">
          <div class="form-group">
            <label>Judul</label>
            <input type="text" class="form-control form-control-sm" name="<?=COL_NM_JUDUL?>" />
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <textarea class="form-control" name="<?=COL_NM_KETERANGAN?>"></textarea>
          </div>
          <div class="form-group">
            <input type="file" name="userfile" />
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;CLOSE</button>
        <button type="button" class="btn btn-primary btn-xs btn-ok"><i class="fa fa-check"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/_js'); ?>
<script>
$(document).ready(function() {
  var modalmateri = $('#modal-materi');

  modalmateri.on('hidden.bs.modal', function (event) {
    $(".modal-title", modalmateri).html('');
    location.reload();
  });

  $('select, input', $('#form-filter')).change(function() {
    $('#form-filter').submit();
  });

  $('.btn-tambah-materi').click(function(){
    var a = $(this);
    var kdTA = $('[name=Kd_TahunAjaran]').val();
    var kdKelas = $('[name=Kd_Kelas]').val();
    var kdMapel = $('[name=Kd_MataPelajaran]').val();
    if(!kdTA) {
      toastr.error('Pilih Tahun Ajaran dahulu.');
      return false;
    }
    if(!kdKelas) {
      toastr.error('Pilih Kelas dahulu.');
      return false;
    }
    if(!kdMapel) {
      toastr.error('Pilih Mata Pelajaran dahulu.');
      return false;
    }
    modalmateri.modal("show");

    $(".btn-ok", modalmateri).unbind('click').click(function() {
      var dis = $(this);
      dis.attr("disabled", true);
      $('#form-materi', modalmateri).ajaxSubmit({
        dataType: 'json',
        url : a.attr('href'),
        success : function(data){
          if(data.error==0){
            modalmateri.modal('hide');
          } else{
            toastr.error(data.error);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          dis.attr("disabled", false);
        }
      });
    });
    return false;
  });

  $('.btn-delete-materi').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.post(url, function(data) {
        if(data.error == 0) {
          location.reload();
        } else {
          toastr.error(data.error);
        }
      }, 'json');
    }
    return false;
  });
});
</script>
<?php $this->load->view('layouts/backend-footer'); ?>
