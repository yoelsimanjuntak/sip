<table class="table table-bordered">
  <tbody>
    <tr>
      <th>Mata Pelajaran</th>
      <th>Pengampu</th>
      <th class="text-center" style="width: 10px; white-space: nowrap;">#</th>
    </tr>
    <?php
    foreach ($rmapel as $m) {
      ?>
      <tr>
        <td><?=$m[COL_NM_MATAPELAJARAN]?></td>
        <td><?=$m[COL_NM_PENGAJAR].' - NIP: '.$m[COL_NM_NOMORINDUKPEGAWAI]?></td>
        <td class="text-center" style="width: 10px; white-space: nowrap;">
          <?=anchor(site_url('management/matapelajaran-delete/'.$m[COL_UNIQ]), '<i class="fa fa-times"></i>&nbsp;HAPUS', array('class'=>'btn btn-xs btn-danger btn-delete'))?>
          </td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>