<?=form_open(site_url('management/pelajar-add/'.$kdTA.'/'.$kdKelas), array('id'=>'form-mapel'))?>
<div class="form-group row">
  <div class="col-sm-12">
    <div class="input-group" style="width: 100%">
      <select name="<?=COL_KD_PELAJAR?>" class="form-control form-sm">
        <?=GetCombobox("select * from mpelajar order by Nm_Pelajar", COL_KD_PELAJAR, array(COL_NM_PELAJAR, COL_NM_NOMORINDUKSISWA))?>
      </select>
      <span class="input-group-append">
        <button type="submit" class="btn btn-outline-success"><i class="fa fa-plus"></i>&nbsp;TAMBAH</button>
      </span>
    </div>
  </div>
</div>
<?=form_close()?>
<div class="row">
  <div id="div-mapel" class="col-sm-12">

  </div>
</div>

<script>
  function loadMapel() {
    $('#div-mapel').empty();
    $('#div-mapel').load('<?=site_url("management/pelajar-partial/".$kdTA."/".$kdKelas)?>', function(){
      $('.btn-delete', $('#div-mapel')).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.post(url, function(data) {
            if(data.error == 0) {
              loadMapel();
            } else {
              toastr.error(data.error);
            }
          }, 'json');
        }
        return false;
      });
    });
  }
  $(document).ready(function() {
    loadMapel();
    $('select', $('#form-mapel')).not('.no-select2').select2({ width: 'resolve', theme: 'bootstrap4' });
    $('#form-mapel').submit(function() {
      $(this).ajaxSubmit({
        dataType: 'json',
        type : 'post',
          success: function(data){
            if(data.error == 0) {
              loadMapel();
            } else {
              toastr.error(data.error);
            }
          },
          error: function(a,b,c){
            toastr.error('Response Error');
          }
      });
      return false;
    });
  });
</script>
