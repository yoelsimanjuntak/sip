<?php $this->load->view('layouts/backend-header'); ?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <?=form_open(current_url(), array('id'=>'form-filter', 'method'=>'get'))?>
            <div class="form-group row mb-0">
              <label class="control-label col-sm-2 text-left">Tahun Ajaran</label>
              <div class="col-sm-2">
                <select name="<?=COL_KD_TAHUNAJARAN?>" class="form-control form-sm">
                  <?=GetCombobox("select * from mtahunajaran order by Nm_TahunAjaran", COL_KD_TAHUNAJARAN, COL_NM_TAHUNAJARAN, (!empty($kdTA)?$kdTA:null), true, false, '-- Tahun Ajaran --')?>
                </select>
              </div>
            </div>
            <?=form_close()?>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <th>Kelas</th>
                      <th class="text-center">Jlh. Mata Pelajaran</th>
                      <th class="text-center">Jlh. Pelajar</th>
                      <th class="text-center">Jadwal</th>
                    </tr>
                    <?php
                    foreach ($rkelas as $kel) {
                      ?>
                      <tr>
                        <td><?=$kel[COL_NM_KELAS]?></td>
                        <td class="text-center">
                          <a href="<?=site_url('management/matapelajaran/'.$kdTA.'/'.$kel[COL_KD_KELAS])?>" class="link-popup" data-title="Mata Pelajaran">
                            <?=number_format($kel['count_mapel'])?>
                          </a>
                        </td>
                        <td class="text-center">
                          <a href="<?=site_url('management/pelajar/'.$kdTA.'/'.$kel[COL_KD_KELAS])?>" class="link-popup" data-title="Alokasi Pelajar">
                            <?=number_format($kel['count_pelajar'])?>
                          </a>
                        </td>
                        <td class="text-center">
                          <a href="<?=site_url('management/jadwal').'?Kd_TahunAjaran='.$kdTA.'&Kd_Kelas='.$kel[COL_KD_KELAS]?>">
                            <?=number_format($kel['count_jadwal'])?>
                          </a>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-popup" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light"></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;CLOSE</button>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/_js'); ?>
<script>
$(document).ready(function() {
  var modalpop = $('#modal-popup');

  modalpop.on('hidden.bs.modal', function (event) {
    location.reload();
    //$(this).find(".modal-body").empty();
    //$(this).find(".modal-title").empty();
  });

  $('select, input', $('#form-filter')).change(function() {
    $('#form-filter').submit();
  });

  $('.link-popup').click(function(){
    var title = $(this).data('title');
    var url = $(this).attr('href');

    modalpop.modal('show');
    $('.modal-title', modalpop).html(title);
    $('.modal-body', modalpop).load(url, function() {

    });
    return false;
  });
});
</script>
<?php $this->load->view('layouts/backend-footer'); ?>
