<?php $this->load->view('layouts/backend-header'); ?>
<?php
$ruser = GetLoggedUser();
$condKelas = "";
if($ruser[COL_ROLEID] == ROLEGURU) {
  $mpengajar = $this->db
  ->where(COL_NM_NOMORINDUKPEGAWAI, $ruser[COL_USERNAME])
  ->get(TBL_MPENGAJAR)
  ->row_array();

  if(!empty($kdTA) && !empty($mpengajar)) {
    $arrkelas_ = array();
    $arrkelas = $this->db
    ->select(COL_KD_KELAS)
    ->where(array(
      COL_KD_TAHUNAJARAN=>$kdTA,
      COL_KD_PENGAJAR=>$mpengajar[COL_KD_PENGAJAR]
    ))
    ->group_by(COL_KD_KELAS)
    ->get(TBL_TKELASMATAPELAJARAN)
    ->result_array();
    foreach($arrkelas as $k) {
      $arrkelas_[] = $k[COL_KD_KELAS];
    }
    $arrkelas_ = implode("','", $arrkelas_);
    $condKelas = "where Kd_Kelas in ('$arrkelas_')";
  } else {
    $condKelas = "where 1 != 1";
  }
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <?=form_open(site_url('management/nilai-rekapitulasi'), array('id'=>'form-filter', 'method'=>'get'))?>
            <div class="form-group row mb-0">
              <div class="col-sm-4">
                <label class="control-label">Tahun Ajaran</label>
                <select name="<?=COL_KD_TAHUNAJARAN?>" class="form-control form-sm">
                  <?=GetCombobox("select * from mtahunajaran order by Nm_TahunAjaran", COL_KD_TAHUNAJARAN, COL_NM_TAHUNAJARAN, (!empty($kdTA)?$kdTA:null), true, false, '-- Tahun Ajaran --')?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="control-label">Kelas</label>
                <select name="<?=COL_KD_KELAS?>" class="form-control form-sm" <?=$ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGURU ? 'disabled':''?>>
                  <?=GetCombobox("select * from mkelas $condKelas order by Nm_Kelas", COL_KD_KELAS, COL_NM_KELAS, (!empty($kdKelas)?$kdKelas:null), true, false, '-- Kelas --')?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="control-label">Pelajar</label>
                <select name="<?=COL_KD_PELAJAR?>" class="form-control form-sm" <?=$ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGURU ? 'disabled':''?>>
                  <?=GetCombobox("select * from mpelajar order by Nm_Pelajar", COL_KD_PELAJAR, COL_NM_PELAJAR, (!empty($kdPelajar)?$kdPelajar:null), true, false, '-- Pelajar --')?>
                </select>
              </div>
            </div>
            <?=form_close()?>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <th style="white-space: nowrap;">Mata Pelajaran</th>
                        <?php
                        foreach ($rkomponen as $k) {
                          ?>
                          <th class="text-center" style="width: 150px;"><?=$k[COL_NM_KOMPONEN]?></th>
                          <?php
                        }
                        ?>
                        <th class="text-center" style="width: 150px;">N. AKHIR</th>
                      </tr>
                      <?php
                      $rmapel = $this->db
                      ->join(TBL_MMATAPELAJARAN,TBL_MMATAPELAJARAN.".".COL_KD_MATAPELAJARAN." = ".TBL_TKELASMATAPELAJARAN.".".COL_KD_MATAPELAJARAN,"left")
                      ->where(array(
                        COL_KD_TAHUNAJARAN=>$kdTA,
                        COL_KD_KELAS=>$kdKelas
                      ))
                      ->order_by(TBL_MMATAPELAJARAN.'.'.COL_NM_MATAPELAJARAN)
                      ->group_by(TBL_TKELASMATAPELAJARAN.'.'.COL_KD_MATAPELAJARAN)
                      ->get(TBL_TKELASMATAPELAJARAN)
                      ->result_array();
                      foreach ($rmapel as $m) {
                        ?>
                        <tr>
                          <td style="white-space: nowrap;"><?=$m[COL_NM_MATAPELAJARAN]?></td>
                          <?php
                          $sum = 0;
                          foreach ($rkomponen as $k) {
                            $text = '<span class="text-small text-danger">(kosong)</span>';
                            $rnilai = $this->db
                            ->where(array(
                              COL_KD_TAHUNAJARAN=>$kdTA,
                              COL_KD_KELAS=>$kdKelas,
                              COL_KD_PELAJAR=>$kdPelajar,
                              COL_KD_MATAPELAJARAN=>$m[COL_KD_MATAPELAJARAN],
                              COL_NM_KOMPONEN=>$k[COL_NM_KOMPONEN]
                            ))
                            ->get(TBL_TNILAI)
                            ->row_array();
                            if(!empty($rnilai)) {
                              $sum += (($rnilai[COL_BOBOT]/100)*$rnilai[COL_NILAI]);
                              $text = number_format($rnilai[COL_NILAI],2);
                            }
                            ?>
                            <td class="pt-1 pb-2 text-center"><?=$text?></td>
                            <?php
                          }
                          ?>
                          <td class="pt-1 pb-2 text-center"><?=number_format($sum,2)?></td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-nilai" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title font-weight-light">Nilai</h6>
      </div>
      <div class="modal-body">
         <form id="form-nilai" method="post" action="#">
          <div class="form-group row mb-0">
            <label class="control-label col-sm-3 mb-0">Nilai</label>
            <div class="col-sm-9">
              <input type="text" class="form-control form-control-sm money text-right" name="<?=COL_NILAI?>" />
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;CLOSE</button>
        <button type="button" class="btn btn-primary btn-xs btn-ok"><i class="fa fa-check"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/_js'); ?>
<script>
$(document).ready(function() {
  var modalnilai = $('#modal-nilai');

  modalnilai.on('hidden.bs.modal', function (event) {
    $(".modal-title", modalnilai).html('');
    location.reload();
  });

  $('select, input', $('#form-filter')).change(function() {
    $('#form-filter').submit();
  });

  $('.btn-nilai').click(function(){
    var a = $(this);
    var title = $(this).data('title');
    var nilai = $(this).data('nilai');

    $('[name=<?=COL_NILAI?>]', modalnilai).val(nilai);
    modalnilai.modal("show");

    $(".modal-title", modalnilai).html(title);
    $(".btn-ok", modalnilai).unbind('click').click(function() {
      var dis = $(this);
      dis.attr("disabled", true);
      $('#form-nilai', modalnilai).ajaxSubmit({
        dataType: 'json',
        url : a.attr('href'),
        success : function(data){
          if(data.error==0){
            modalnilai.modal('hide');
          } else{
            toastr.error(data.error);
          }
        },
        complete: function() {
          dis.attr("disabled", false);
          //toastr.error("Server Error.");
        }
      });
    });
    return false;
  });
});
</script>
<?php $this->load->view('layouts/backend-footer'); ?>
