<?php $this->load->view('header-front') ?>
<?php
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        date('d-m-Y', strtotime($d[COL_POSTDATE])),
        anchor('post/view/'.$d[COL_POSTSLUG],$d[COL_POSTTITLE]),
        $d[COL_POSTCATEGORYNAME],
        $d[COL_TOTALVIEW]
    );
    $i++;
}
$data = json_encode($res);
?>
<div class="row">
    <div class="col-sm-8">
        <div class="box box-default">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover">

                    </table>
                </form>
            </div>
        </div>
    </div>
    <?php $this->load->view('sidebar-front') ?>
</div>
<?php $this->load->view('footer-front') ?>
<script type="text/javascript">
    $(document).ready(function() {
        var dataTable = $('#datalist').dataTable({
            //"sDom": "Rlfrtip",
            "aaData": <?=$data?>,
            //"bJQueryUI": true,
            //"aaSorting" : [[5,'desc']],
            "scrollY" : '40vh',
            "iDisplayLength": 100,
            "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
            "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            "order": [[ 0, "desc" ]],
            "aoColumns": [
                {"sTitle": "Tanggal"},
                {"sTitle": "Judul"},
                {"sTitle": "Kategori"},
                {"sTitle": "Dilihat", "width": "50px"}
            ]
        });
    });
</script>