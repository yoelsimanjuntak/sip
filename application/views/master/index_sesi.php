<?php
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
      '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_KD_SESI] . '" />',
      anchor('master/sesi-edit/'.$d[COL_KD_SESI],$d[COL_NM_SESI],array('class' => 'modal-popup-edit', 'data-name' => $d[COL_NM_SESI], 'data-from' => $d[COL_JAM_FROM], 'data-to' => $d[COL_JAM_TO])),
      "<strong>".$d[COL_JAM_FROM]."</strong> s.d <strong>".$d[COL_JAM_TO]."</strong>"
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>
<?php $this->load->view('layouts/backend-header'); ?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Data</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <p>
            <?=anchor('master/sesi-delete','<i class="fa fa-trash"></i> DELETE',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
            <?=anchor('master/sesi-add','<i class="fa fa-plus"></i> CREATE',array('class'=>'modal-popup btn btn-primary btn-sm'))?>
        </p>

        <div class="card card-default">
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover">

              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-editor" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Editor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-editor" method="post" action="#">
          <div class="form-group">
              <label>Sesi</label>
              <div class="input-group">
                <input type="text" class="form-control" placeholder="cth: Sesi 1" name="<?=COL_NM_SESI?>" required />
              </div>
          </div>
          <div class="form-group">
              <label>Jam</label>
              <div class="input-group">
                <input type="text" class="form-control" placeholder="cth: 08:00" name="<?=COL_JAM_FROM?>" required />
                <label class="control-label pl-2 pr-2">s.d</label>
                <input type="text" class="form-control" placeholder="cth: 08:00" name="<?=COL_JAM_TO?>" required />
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;CANCEL</button>
        <button type="button" class="btn btn-primary btn-ok"><i class="fa fa-check"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/_js'); ?>
<script type="text/javascript">
$(document).ready(function() {
    var dataTable = $('#datalist').dataTable({
      "autoWidth" : false,
      //"sDom": "Rlfrtip",
      "aaData": <?=$data?>,
      //"bJQueryUI": true,
      //"aaSorting" : [[5,'desc']],
      "scrollY" : '40vh',
      "scrollX": "120%",
      "iDisplayLength": 100,
      "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
      //"dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
      "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
      "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
      "order": [[ 2, "asc" ]],
      "aoColumns": [
          {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" />", "width": "10px","bSortable":false},
          {"sTitle": "Sesi"},
          {"sTitle": "Jam"}
      ],
      "fnCreatedRow" : function( nRow, aData, iDataIndex) {
        $('.modal-popup-edit', nRow).click(function(){
          var a = $(this);
          var name = $(this).data('name');
          var from = $(this).data('from');
          var to = $(this).data('to');
          var editor = $("#modal-editor");

          $('[name=<?=COL_NM_SESI?>]', editor).val(name);
          $('[name=<?=COL_JAM_FROM?>]', editor).val(from);
          $('[name=<?=COL_JAM_TO?>]', editor).val(to);
          editor.modal("show");
          $(".btn-ok", editor).unbind('click').click(function() {
            var dis = $(this);
            dis.attr("disabled", true);
            $('#form-editor').ajaxSubmit({
              dataType: 'json',
              url : a.attr('href'),
              success : function(data){
                if(data.error==0){
                    window.location.reload();
                }else{
                    $(".error-message", editor).html(data.error);
                }
              },
              complete: function() {
                dis.attr("disabled", false);
                //toastr.error("Server Error.");
              }
            });
          });
          return false;
        });
      }
    });
    $('#cekbox').click(function(){
      if($(this).is(':checked')){
        $('.cekbox').prop('checked',true);
      }else{
        $('.cekbox').prop('checked',false);
      }
    });

    $('.modal-popup').click(function(){
      var a = $(this);
      var name = $(this).data('name');
      var from = $(this).data('from');
      var to = $(this).data('to');
      var editor = $("#modal-editor");

      $('[name=<?=COL_NM_KELAS?>]', editor).val(name);
      $('[name=<?=COL_JAM_FROM?>]', editor).val(from);
      $('[name=<?=COL_JAM_TO?>]', editor).val(to);
      editor.modal("show");
      $(".btn-ok", editor).unbind('click').click(function() {
        var dis = $(this);
        dis.attr("disabled", true);
        $('#form-editor').ajaxSubmit({
          dataType: 'json',
          url : a.attr('href'),
          success : function(data){
            if(data.error==0){
                window.location.reload();
            }else{
                $(".error-message", editor).html(data.error);
            }
          },
          complete: function() {
            dis.attr("disabled", false);
            //toastr.error("Server Error.");
          }
        });
      });
      return false;
    });
});
</script>
<?php $this->load->view('layouts/backend-footer'); ?>
