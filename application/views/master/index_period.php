<?php
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_KD_PERIODE] . '" />',
        anchor('master/period-edit/'.$d[COL_KD_PERIODE],'<i class="fa fa-pencil"></i>'),
        $d[COL_TAHUNFROM]." s.d ".$d[COL_TAHUNTO],
        $d[COL_NM_PIMPINAN],
        $d[COL_NM_WAKILPIMPINAN]
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>
<?php $this->load->view('layouts/backend-header'); ?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Data</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <p>
            <?=anchor('master/period-delete','<i class="fa fa-close"></i> DELETE',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
            <?=anchor('master/period-add','<i class="fa fa-plus"></i> CREATE',array('class'=>'btn btn-primary btn-sm'))?>
        </p>

        <div class="card card-default">
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover">

              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('layouts/_js'); ?>
<script type="text/javascript">
$(document).ready(function() {
    var dataTable = $('#datalist').dataTable({
      "autoWidth" : false,
      //"sDom": "Rlfrtip",
      "aaData": <?=$data?>,
      //"bJQueryUI": true,
      //"aaSorting" : [[5,'desc']],
      "scrollY" : '40vh',
      "scrollX": "120%",
      "iDisplayLength": 100,
      "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
      //"dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
      "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
      "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
      "order": [[ 2, "desc" ]],
      "aoColumns": [
          {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />", "width": "10px","bSortable":false},
          {"sTitle": "#", "width": "10px","bSortable":false},
          {"sTitle": "Periode"},
          {"sTitle": "Nama Pimpinan"},
          {"sTitle": "Nama Wakil Pimpinan"}
      ]
    });
    $('#cekbox').click(function(){
      if($(this).is(':checked')){
        $('.cekbox').prop('checked',true);
      }else{
        $('.cekbox').prop('checked',false);
      }
    });
});
</script>
<?php $this->load->view('layouts/backend-footer'); ?>
