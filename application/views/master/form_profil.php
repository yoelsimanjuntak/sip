<?php $this->load->view('layouts/backend-header'); ?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('master/profil')?>"> <?=$title?></a></li>
          <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <?php
        if($this->input->get('error') == 1) {
          ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;Data gagal disimpan, silahkan coba kembali.</span>
          </div>
          <?php
        }
        if(validation_errors()) {
          ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=validation_errors()?></span>
          </div>
          <?php
        }
        if(!empty($upload_errors)) {
          ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=$upload_errors?></span>
          </div>
          <?php
        }
        ?>
        <div class="card card-primary">
          <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group row">
                  <label class="control-label col-sm-3">Nama Institusi</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Nama Institusi" name="<?=COL_NM_INSTITUSI?>" value="<?= $edit ? $data[COL_NM_INSTITUSI] : ""?>" required />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-3">No. Telp / Email</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="No. Telp / Fax" name="<?=COL_NM_PHONENO?>" value="<?= $edit ? $data[COL_NM_PHONENO] : ""?>" />
                  </div>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Email" name="<?=COL_NM_EMAIL?>" value="<?= $edit ? $data[COL_NM_EMAIL] : ""?>" />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-3">Alamat</label>
                  <div class="col-sm-8">
                    <textarea class="form-control" rows="5" placeholder="Alamat Lengkap" name="<?=COL_NM_ALAMAT?>" required="true"><?= $edit ? $data[COL_NM_ALAMAT] : ""?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-3">Profil</label>
                  <div class="col-sm-8">
                    <textarea class="form-control" rows="5" placeholder="Keterangan Singkat" name="<?=COL_NM_BIO?>" required="true"><?= $edit ? $data[COL_NM_BIO] : ""?></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="row" style="text-align: center">
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary">SIMPAN</button>
              </div>
            </div>
          </div>
          <?=form_close()?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/_js'); ?>
<?php $this->load->view('layouts/backend-footer'); ?>
