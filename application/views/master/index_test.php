<html>
<head>
  <title>TEST</title>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
</head>
<body>
  <table id="datalist" border="1">

  </table>
  <script type="text/javascript">
  $(document).ready(function() {
    var dataTable = $('#datalist').dataTable({
      "autoWidth" : false,
      "aaData": [['C1R1','C2R1'],['C1R2','C2R2']],
      "scrollY" : '44vh',
      "scrollX": "120%",
      "iDisplayLength": 100,
      "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
      //"dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
      //"buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
      "order": [[ 1, "asc" ]],
      "aoColumns": [
          {"sTitle": "Col 1", "width":"40 0px"},
          {"sTitle": "Col 2"}
      ]
    });
  });
  </script>
</body>
