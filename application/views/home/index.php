<?php $this->load->view('layouts/frontend-header') ?>
<?php
$ruser = GetLoggedUser();
 ?>
 <style>
 .timeline::before {
   display: none;
 }
 </style>
<div class="content-header">
    <div class="container">
        <!--<div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?></h1>
            </div>
        </div>-->
    </div>
</div>

<div class="content">
    <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <?php
            $rprofil = $this->db->get(TBL_MPROFIL)->row_array();
            $rperiod = $this->db->order_by(COL_TAHUNFROM, 'desc')->get(TBL_MPERIODE)->row_array();
            $rTahunAjaran = $this->db
            ->where(COL_IS_ACTIVE, 1)
            ->order_by(COL_NM_TAHUNAJARAN, 'desc')
            ->get(TBL_MTAHUNAJARAN)
            ->row_array();
            $rpengajar = $this->db
            ->get(TBL_MPENGAJAR)
            ->result_array();
            $rkelas = $this->db
            ->get(TBL_MKELAS)
            ->result_array();
            $rmapel = $this->db
            ->where(array(
              COL_KD_TAHUNAJARAN=>!empty($rTahunAjaran)?$rTahunAjaran[COL_KD_TAHUNAJARAN]:-999
            ))
            ->group_by(COL_KD_MATAPELAJARAN)
            ->get(TBL_TKELASMATAPELAJARAN)
            ->result_array();
            $rpelajar = $this->db
            ->where(array(
              COL_KD_TAHUNAJARAN=>!empty($rTahunAjaran)?$rTahunAjaran[COL_KD_TAHUNAJARAN]:-999
            ))
            ->group_by(COL_KD_PELAJAR)
            ->get(TBL_TKELASPELAJAR)
            ->result_array();
            ?>
            <div class="card card-outline card-olive">
              <div class="card-header">
                  <h5 class="card-title m-0">Selamat Datang di <strong><?=!empty($rprofil)?$rprofil[COL_NM_INSTITUSI]:'-'?></strong></h5>
              </div>
              <div class="card-body p-0">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <td style="width: 10px; white-space: nowrap;">Pimpinan</td>
                      <td style="width: 10px; white-space: nowrap;">:</td>
                      <td class="font-weight-bold"><?=!empty($rperiod)?$rperiod[COL_NM_PIMPINAN]:'-'?></td>
                    </tr>
                    <tr>
                      <td style="width: 10px; white-space: nowrap;">Wakil Pimpinan</td>
                      <td style="width: 10px; white-space: nowrap;">:</td>
                      <td class="font-weight-bold"><?=!empty($rperiod)?$rperiod[COL_NM_WAKILPIMPINAN]:'-'?></td>
                    </tr>
                    <tr>
                      <td style="width: 10px; white-space: nowrap;">Alamat</td>
                      <td style="width: 10px; white-space: nowrap;">:</td>
                      <td class="font-weight-bold"><?=!empty($rprofil)?$rprofil[COL_NM_ALAMAT]:'-'?></td>
                    </tr>
                    <tr>
                      <td style="width: 10px; white-space: nowrap;">Email</td>
                      <td style="width: 10px; white-space: nowrap;">:</td>
                      <td class="font-weight-bold"><?=!empty($rprofil)?$rprofil[COL_NM_EMAIL]:'-'?></td>
                    </tr>
                    <tr>
                      <td style="width: 10px; white-space: nowrap;">No. Telp</td>
                      <td style="width: 10px; white-space: nowrap;">:</td>
                      <td class="font-weight-bold"><?=!empty($rprofil)?$rprofil[COL_NM_PHONENO]:'-'?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <div class="card card-outline card-primary">
              <div class="card-header">
                  <h5 class="card-title m-0">Statistik T.A <strong><?=!empty($rTahunAjaran)?$rTahunAjaran[COL_NM_TAHUNAJARAN]:'-'?></strong></h5>
              </div>
              <div class="card-body p-0">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <td style="width: 10px; white-space: nowrap;">Jlh. Kelas</td>
                      <td style="width: 10px; white-space: nowrap;">:</td>
                      <td class="font-weight-bold text-right"><?=number_format(count($rkelas))?></td>
                      <td class="text-sm" style="width: 10px; white-space: nowrap;">KELAS</td>
                    </tr>
                    <tr>
                      <td style="width: 10px; white-space: nowrap;">Jlh. Mata Pelajaran</td>
                      <td style="width: 10px; white-space: nowrap;">:</td>
                      <td class="font-weight-bold text-right"><?=number_format(count($rmapel))?></td>
                      <td class="text-sm" style="width: 10px; white-space: nowrap;">PELAJARAN</td>
                    </tr>
                    <tr>
                      <td style="width: 10px; white-space: nowrap;">Jlh. Pengajar</td>
                      <td style="width: 10px; white-space: nowrap;">:</td>
                      <td class="font-weight-bold text-right"><?=number_format(count($rpengajar))?></td>
                      <td class="text-sm" style="width: 10px; white-space: nowrap;">ORANG</td>
                    </tr>
                    <tr>
                      <td style="width: 10px; white-space: nowrap;">Jlh. Siswa</td>
                      <td style="width: 10px; white-space: nowrap;">:</td>
                      <td class="font-weight-bold text-right"><?=number_format(count($rpelajar))?></td>
                      <td class="text-sm" style="width: 10px; white-space: nowrap;">ORANG</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="card card-outline card-olive">
                <div class="card-header">
                    <h5 class="card-title m-0">Berita</h5>
                </div>
                <div class="card-body">
                  <div class="row">
                    <?php
                    $rberita = $this->mpost->search(10,"",1);
                    if(!empty($rberita)) {
                      ?>
                      <div class="timeline timeline-inverse mb-0 w-100">
                        <?php
                        foreach ($rberita as $r) {
                          ?>
                          <div class="row m-0 mb-2">
                            <div class="timeline-item m-0 w-100">
                              <span class="time"><i class="fa fa-calendar"></i> <?=date('Y-m-d', strtotime($r[COL_CREATEDON]))?></span>
                              <h3 class="timeline-header font-weight-bold"><?=anchor(site_url('home/page/'.$r[COL_POSTSLUG]), $r[COL_POSTTITLE])?></h3>
                              <div class="timeline-body">
                                <?php
                                $rimages = $this->db->where(COL_POSTID, $r[COL_POSTID])->limit(4)->get(TBL_POSTIMAGES)->result_array();
                                if(!empty($rimages)) {
                                  ?>
                                  <div class="row">
                                    <div class="col-sm-12 text-center">
                                      <?php
                                      foreach ($rimages as $img) {
                                        ?>
                                        <a href="<?=MY_UPLOADURL.$img[COL_FILENAME]?>" data-toggle="lightbox" data-title="<?=$r[COL_POSTTITLE]?>" data-gallery="gallery">
                                          <img src="<?=MY_UPLOADURL.$img[COL_FILENAME]?>" class="elevation-1 mb-2" width="300px" style="max-width: 100%" />
                                        </a>
                                        <?php
                                      }
                                      ?>
                                    </div>
                                  </div>
                                  <?php
                                }
                                ?>
                                <br  />
                                <?php
                                $strippedcontent = strip_tags($r[COL_POSTCONTENT]);
                                ?>
                                <?=strlen($strippedcontent) > 400 ? substr($strippedcontent, 0, 400) . "..." : $strippedcontent ?>
                              </div>
                            </div>
                          </div>
                          <?php
                        }
                        ?>
                      </div>
                      <?php
                    } else {
                      ?>
                      <p class="font-italic">Belum ada data.</p>
                      <?php
                    }
                    ?>
                  </div>
                </div>
                <div class="card-footer text-right">
                  <a href="<?=site_url('home/post/1')?>" class="btn btn-outline-success btn-sm"><i class="fa fa-list"></i> SELENGKAPNYA (<strong><?=number_format(count($rberita))?></strong>)</a>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-popup" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Jadwal Kunjungan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-close"></i></span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">CANCEL</button>
                <button type="button" class="btn btn-primary btn-flat btn-ok">SUBMIT</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php $this->load->view('layouts/_js') ?>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<script type="text/javascript">
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox({
    alwaysShowClose: true
  });
});

$('.btn-popup').click(function(){
    var url = $(this).attr('href');
    var modal = $("#modal-popup");
    var modalBody = $(".modal-body", modal);

    modal.on('shown.bs.modal', function (e) {
      $('select', modalBody).not('.no-select2').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
    modal.on('hidden.bs.modal', function (e) {
      $('.btn-ok', modalBody).html("SUBMIT").attr("disabled", false);
    });
    modal.modal("show");

    modalBody.load(url, function() {

    });
    $(".btn-ok", modal).unbind('click').click(function() {
      var dis = $(this);
      dis.html("Loading...").attr("disabled", true);
      $('form', modalBody).ajaxSubmit({
          dataType: 'json',
          url : url,
          success : function(data){
            if(data.error != 0){
                toastr.error(data.error);
            }else{
                toastr.success("Jadwal berhasil ditambahkan.");
            }
          },
          complete: function() {
            dis.html("SUBMIT").attr("disabled", false);
          }
      });
    });
    return false;
});
</script>
<?php $this->load->view('layouts/frontend-footer') ?>
