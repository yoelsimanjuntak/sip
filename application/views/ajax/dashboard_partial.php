<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 9/26/2019
 * Time: 4:48 PM
 */
?>
<?php
$ruser = GetLoggedUser();
$cond = array();
if(!empty($kdkec)) $cond[TBL_MKECAMATAN.".".COL_KD_KECAMATAN] = $kdkec;
if(!empty($kdkel)) $cond[TBL_MKELURAHAN.".".COL_KD_KELURAHAN] = $kdkel;
if(!empty($kdpoktan)) $cond[TBL_MKELTAN.".".COL_KD_KELOMPOKTANI] = $kdpoktan;

$arrkel = array();
if($ruser[COL_ROLEID] == ROLEPPL) {
    $rkel = $this->db->where(COL_KD_PPL, $ruser[COL_COMPANYID])->get(TBL_MPPL_KELURAHAN)->result_array();

    foreach($rkel as $k) {
        $arrkel[] = $k[COL_KD_KELURAHAN];
    }
}
if($ruser[COL_ROLEID] == ROLEPPS) {
    $rpps = $this->db->where(COL_KD_PPS, $ruser[COL_COMPANYID])->get(TBL_MPPS)->row_array();
    if(!empty($rpps)) {
        $arrkel[] = $rpps[COL_KD_KELURAHAN];
    }
}
if($opt == 1) {
    /*$q = $this->db
        ->where($cond)
        ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
        ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left");
    if(count($arrkel) > 0) {
        $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
    }
    $count_keltan = $q->get(TBL_MKELTAN)->num_rows();*/

    $q = $this->db
        ->where($cond)
        ->join(TBL_MKELTAN,TBL_MKELTAN.'.'.COL_KD_KELOMPOKTANI." = ".TBL_MKELTAN_ANGGOTA.".".COL_KD_KELOMPOKTANI,"left")
        ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
        ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left");
    if(count($arrkel) > 0) {
        $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
    }
    $count_keltan_anggota = $q->get(TBL_MKELTAN_ANGGOTA)->num_rows();

    $q = $this->db
        ->select_sum(COL_VOLUME)
        ->where($cond)
        ->join(TBL_MKELTAN_ANGGOTA,TBL_MKELTAN_ANGGOTA.'.'.COL_KD_ANGGOTA." = ".TBL_MKELTAN_KOMODITAS.".".COL_KD_ANGGOTA,"left")
        ->join(TBL_MKELTAN,TBL_MKELTAN.'.'.COL_KD_KELOMPOKTANI." = ".TBL_MKELTAN_ANGGOTA.".".COL_KD_KELOMPOKTANI,"left")
        ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
        ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left");
    if(count($arrkel) > 0) {
        $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
    }
    $sum_keltan_luas = $q->get(TBL_MKELTAN_KOMODITAS)->row_array();

    $q = $this->db
        ->distinct(TBL_MKELTAN_KOMODITAS.".".COL_KD_KOMODITAS)
        ->where($cond)
        ->join(TBL_MKELTAN_ANGGOTA,TBL_MKELTAN_ANGGOTA.'.'.COL_KD_ANGGOTA." = ".TBL_MKELTAN_KOMODITAS.".".COL_KD_ANGGOTA,"left")
        ->join(TBL_MKELTAN,TBL_MKELTAN.'.'.COL_KD_KELOMPOKTANI." = ".TBL_MKELTAN_ANGGOTA.".".COL_KD_KELOMPOKTANI,"left")
        ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
        ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left");
    if(count($arrkel) > 0) {
        $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
    }
    $count_keltan_komoditas = $q->get(TBL_MKELTAN_KOMODITAS)->num_rows();

    $q = $this->db
        ->where($cond)
        ->where(COL_KD_TAHUN, date('Y'))
        ->join(TBL_MKELTAN,TBL_MKELTAN.'.'.COL_KD_KELOMPOKTANI." = ".TBL_TKELTAN_BANTUAN.".".COL_KD_KELOMPOKTANI,"left")
        ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
        ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left");
    if(count($arrkel) > 0) {
        $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
    }
    $count_keltan_bantuan = $q->get(TBL_TKELTAN_BANTUAN)->num_rows();
    ?>

    <div class="widget-user-header bg-aqua" style="padding: 15px; height: 96px">
        <h3 class="widget-user-username">Kelompok Tani</h3>
        <h5 class="widget-user-desc">
            <a style="color: #fff;" href="<?=site_url('master/kelompok-tani-anggota')?>">
                <span style="font-size: 24pt"><?=number_format($count_keltan_anggota, 0)?></span>
                &nbsp;&nbsp;Anggota / NIK
                &nbsp;&nbsp;<i class="fa fa-angle-double-right"></i>
            </a>

        </h5>
        <div class="icon">
            <i class="ion ion-home"></i>
        </div>
    </div>

    <div class="box-footer no-padding">
        <ul class="nav nav-stacked">
            <!--<li><a href="#">Anggota <span class="pull-right badge bg-aqua"><?=number_format($count_keltan_anggota, 0)?></span></a></li>-->
            <li><a href="#">Luas Tanam (Rante) <span class="pull-right badge bg-aqua"><?=number_format($sum_keltan_luas[COL_VOLUME], 0)?></span></a></li>
            <li><a href="#">Komoditas <span class="pull-right badge bg-gray"><?=number_format($count_keltan_komoditas, 0)?></span></a></li>
            <li><a href="#">Bantuan TH. <?=date('Y')?> <span class="pull-right badge bg-aqua"><?=number_format($count_keltan_bantuan, 0)?></span></a></li>
        </ul>
    </div>
    <?php
} else if($opt == 2) {

    $q = $this->db
        ->select_sum(COL_JLH_PUPUK)
        ->where($cond)
        ->join(TBL_MKELTAN_ANGGOTA,TBL_MKELTAN_ANGGOTA.'.'.COL_KD_ANGGOTA." = ".TBL_MKELTAN_PUPUK.".".COL_KD_ANGGOTA,"left")
        ->join(TBL_MKELTAN,TBL_MKELTAN.'.'.COL_KD_KELOMPOKTANI." = ".TBL_MKELTAN_ANGGOTA.".".COL_KD_KELOMPOKTANI,"left")
        ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
        ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left");
    if(count($arrkel) > 0) {
        $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
    }
    $sum_keltan_pupuk = $q->get(TBL_MKELTAN_PUPUK)->row_array();

    $q = $this->db
        ->select("mpupuk.Nm_Pupuk, sum(mkeltan_pupuk.Jlh_pupuk) as sum_pupuk")
        ->where($cond)
        ->join(TBL_MPUPUK,TBL_MPUPUK.'.'.COL_KD_PUPUK." = ".TBL_MKELTAN_PUPUK.".".COL_KD_PUPUK,"left")
        ->join(TBL_MKELTAN_ANGGOTA,TBL_MKELTAN_ANGGOTA.'.'.COL_KD_ANGGOTA." = ".TBL_MKELTAN_PUPUK.".".COL_KD_ANGGOTA,"left")
        ->join(TBL_MKELTAN,TBL_MKELTAN.'.'.COL_KD_KELOMPOKTANI." = ".TBL_MKELTAN_ANGGOTA.".".COL_KD_KELOMPOKTANI,"left")
        ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
        ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left")
        ->group_by(TBL_MPUPUK.".".COL_NM_PUPUK);
    if(count($arrkel) > 0) {
        $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
    }
    $rpupuk = $q->get(TBL_MKELTAN_PUPUK)->result_array();
    ?>
    <div class="widget-user-header bg-yellow" style="padding: 15px; height: 96px">
        <h3 class="widget-user-username">Pengunaan Pupuk</h3>
        <h5 class="widget-user-desc"><span style="font-size: 24pt"><?=number_format($sum_keltan_pupuk[COL_JLH_PUPUK], 0)?></span>&nbsp;&nbsp;Kg</h5>
        <div class="icon">
            <i class="ion ion-flask"></i>
        </div>
    </div>

    <div class="box-footer no-padding">
        <?php
        if(count($rpupuk) > 0) {
            ?>
            <ul class="nav nav-stacked">
                <?php
                $i = 1;
                foreach($rpupuk as $p) {
                    $bg = "bg-yellow";
                    if($i%2==0) {
                        $bg = "bg-gray";
                    }
                    ?>
                    <li><a href="#"><?=$p[COL_NM_PUPUK]?> <span class="pull-right badge <?=$bg?>"><?=number_format($p["sum_pupuk"], 0)?></span></a></li>
                <?php
                    $i++;
                }
                ?>
            </ul>
            <?php
        } else {
            echo "<p style='font-style: italic; padding: 10px'>Tidak ada data tersedia.</p>";
        }
        ?>

    </div>

    <?php
}
?>

<div class="overlay" style="display: none">
    <i class="fa fa-spinner fa-spin"></i>
</div>