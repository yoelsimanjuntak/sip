
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">

    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>

    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


    <!-- datatable reorder _ buttons ext + resp + print -->
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">

    <!-- my css -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/my.css">

    <script>
        $(document).ready(function() {
          toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          };

            $('.btn-login', $('.login-section')).click(function() {
                var form = $('#login-form');
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            toastr.error(data.error);
                        }else{
                            location.reload();
                        }
                    },
                    error : function(a,b,c){
                        toastr.error('Response Error');
                    }
                });
            });
        });
    </script>
</head>
<body class="hold-transition layout-top-nav">
<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_NM_FULLNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'user.jpg';
if($ruser) {
    $displaypicture = $ruser[COL_NM_IMAGELOCATION] ? MY_UPLOADURL.$ruser[COL_NM_IMAGELOCATION] : MY_IMAGEURL.'user.jpg';
}
?>
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-light navbar-white">
        <div class="container">
            <a href="<?=site_url()?>" class="navbar-brand">
                <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image elevation-2" style="opacity: .8">
                <span class="brand-text font-weight-light"><?=$this->setting_web_name?> <sup>Ver <?=$this->setting_web_version?></sup></span>
            </a>

            <ul class="navbar-nav ml-auto">
                <?php
                if(IsLogin()) {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link text-success" href="<?=site_url('user/login')?>" title="Login">
                            <i class="fa fa-tachometer"></i>&nbsp;<small>DASHBOARD</small>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=site_url('user/logout')?>" class="nav-link text-secondary">
                            <i class="fa fa-sign-out"></i>&nbsp;<small>KELUAR</small>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item dropdown user-menu">
                        <a class="nav-link text-success" href="<?=site_url('user/login')?>" title="Login">
                            <i class="fa fa-sign-in"></i>&nbsp;<small>LOGIN</small>
                        </a>
                        <!--<ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right login-section">
                            <li class="user-header bg-default">
                                <?= form_open(site_url('user/login-guest'),array('id'=>'login-form')) ?>
                                <p class="text-left" style="margin-bottom: 5px; text-decoration: underline">Login</p>
                                <p class="text-left" style="margin-bottom: 10px; margin-top: 5px;">
                                    <small>Belum punya akun? Silakan <a href="<?=site_url('user/register')?>">daftar</a>.</small>
                                </p>
                                <div>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control form-control-sm" name="<?=COL_USERNAME?>" placeholder="Username" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-user"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="password" class="form-control form-control-sm" name="<?=COL_PASSWORD?>" placeholder="Password" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-key"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?= form_close(); ?>
                            </li>
                            <li class="user-footer">
                                <button type="button" class="btn btn-primary btn-flat btn-login">Login</button>
                            </li>
                        </ul>-->
                    </li>
                    <!--&nbsp;
                    <li class="nav-item dropdown user-menu">
                      <a class="nav-link btn-warning" href="<?=site_url('user/register')?>" title="Login" style="border-radius: .2rem;">
                          <i class="fa fa-user-plus"></i>&nbsp;Daftar
                      </a>
                    </li>-->
                    <?php
                }
                ?>
            </ul>
        </div>
    </nav>
    <div class="content-wrapper">
