<?php $this->load->view('layouts/frontend-header') ?>
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Beranda</a></li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div id="card-service" class="card">
                    <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'order-form','class'=>'form-horizontal'))?>
                    <div class="card-header">
                        <h5 class="card-title m-0">Silakan isi form untuk mendaftar.</h5>
                    </div>
                    <div class="card-body">
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i> Error :
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('error') == 1){
                            ?>
                            <div class="alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                <span class="">Data gagal disimpan, silahkan coba kembali.</span>
                            </div>
                        <?php
                        }
                        if(validation_errors()){
                            ?>
                            <div class="alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                <?=validation_errors()?>
                            </div>
                        <?php
                        }
                        if(!empty($upload_errors)) {
                            ?>
                            <div class="alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                <?=$upload_errors?>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group row">
                                  <label class="control-label col-sm-4">Daftar sebagai</label>
                                  <div class="col-sm-2">
                                    <div class="form-check">
                                      <input id="RadioRoleMember" class="form-check-input" type="radio" name="<?=COL_ROLEID?>" value="<?=ROLEMEMBER?>" checked>
                                      <label class="form-check-label" for="RadioRoleMember">Member</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-2">
                                    <div class="form-check">
                                      <input id="RadioRolePsikolog" class="form-check-input" type="radio" name="<?=COL_ROLEID?>" value="<?=ROLEPSIKOLOG?>">
                                      <label class="form-check-label" for="RadioRolePsikolog">Psikolog</label>
                                    </div>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label col-sm-4">Email</label>
                                  <div class="col-sm-6">
                                      <input type="email" class="form-control" name="<?=COL_NM_EMAIL?>" placeholder="youremail@psikolog.id" required />
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label col-sm-4">Password</label>
                                  <div class="col-sm-6">
                                      <input type="password" class="form-control" name="<?=COL_PASSWORD?>" required />
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label col-sm-4">Konfirmasi Password</label>
                                  <div class="col-sm-6">
                                      <input type="password" class="form-control" name="RepeatPassword" required />
                                  </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-4" for="avatarFile">Avatar (opsional)</label>
                                <div class="col-sm-6">
                                  <div class="input-group">
                                    <div class="custom-file">
                                      <input id="avatarFile" type="file" class="custom-file-input" name="avatar" accept="image/*">
                                      <label class="custom-file-label" for="avatarFile">Pilih file</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6 pl-2">
                              <div class="form-group row">
                                  <label class="control-label col-sm-4">Nama Lengkap</label>
                                  <div class="col-sm-8">
                                      <input type="text" class="form-control" name="<?=COL_NM_FULLNAME?>" placeholder="John Doe" required>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label col-sm-4">Jenis Kelamin</label>
                                  <div class="col-sm-2">
                                    <div class="form-check">
                                      <input id="RadioGenderLK" class="form-check-input" type="radio" name="<?=COL_NM_GENDER?>" value="Pria" checked>
                                      <label class="form-check-label" for="RadioGenderLK">Pria</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-2">
                                    <div class="form-check">
                                      <input id="RadioGenderPR" class="form-check-input" type="radio" name="<?=COL_NM_GENDER?>" value="Wanita">
                                      <label class="form-check-label" for="RadioGenderPR">Wanita</label>
                                    </div>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label col-sm-4">Tanggal Lahir</label>
                                  <div class="col-sm-8">
                                      <input type="text" class="form-control datepicker" name="<?=COL_DATE_BIRTH?>" placeholder="dd-mm-yyyy" required />
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label col-sm-4">No. Telp / HP / WA</label>
                                  <div class="col-sm-8">
                                      <input type="text" class="form-control" name="<?=COL_NM_PHONENO?>" placeholder="081x-xxxx-xxxx" required />
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label col-sm-4">Alamat</label>
                                  <div class="col-sm-8">
                                      <textarea class="form-control" rows="3" name="<?=COL_NM_ADDRESS?>" required="true"></textarea>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label col-sm-4">Bio</label>
                                  <div class="col-sm-8">
                                      <textarea class="form-control" rows="3" name="<?=COL_NM_ABOUT?>" required="true"></textarea>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">SUBMIT</button>
                            </div>
                        </div>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('layouts/_js') ?>
<?php $this->load->view('layouts/frontend-footer') ?>
