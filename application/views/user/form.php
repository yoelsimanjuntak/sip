<?php $this->load->view('layouts/backend-header') ?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url('user/index/'.$role)?>"> Pengguna</a></li>
            <li class="breadcrumb-item active"><?=$edit?'Ubah':'Tambah'?></li>
          </ol>
        </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <a href="<?=site_url('user/index/'.$role)?>" class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i>&nbsp;KEMBALI</a>
          <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i>&nbsp;SIMPAN</button>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Akun</h5>
          </div>
          <div class="card-body">
            <?php
            if($role==ROLEGURU) {
              ?>
              <div class="form-group">
                <label>Pengajar</label>
                <select class="form-control" name="<?=COL_USERNAME?>" required <?=$edit?"disabled":""?>>
                  <?=GetCombobox("SELECT * FROM mpengajar ORDER BY Nm_Pengajar", COL_NM_NOMORINDUKPEGAWAI, array(COL_NM_PENGAJAR, COL_NM_NOMORINDUKPEGAWAI), (!empty($data)?$data[COL_USERNAME]:null))?>
                </select>
              </div>
              <?php
            } else if ($role==ROLESISWA) {
              ?>
              <div class="form-group">
                <label>Pelajar</label>
                <select class="form-control" name="<?=COL_USERNAME?>" required <?=$edit?"disabled":""?>>
                  <?=GetCombobox("SELECT * FROM mpelajar ORDER BY Nm_Pelajar", COL_NM_NOMORINDUKSISWA, array(COL_NM_PELAJAR, COL_NM_NOMORINDUKSISWA), (!empty($data)?$data[COL_USERNAME]:null))?>
                </select>
              </div>
              <?php
            } else {
              ?>
              <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" name="<?=COL_USERNAME?>" value="<?=!empty($data)?$data[COL_USERNAME]:""?>" />
              </div>
              <?php
            }
            ?>
            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" name="<?=COL_NM_EMAIL?>" value="<?=!empty($data)?$data[COL_NM_EMAIL]:""?>" />
            </div>
            <div class="form-group">
              <label>No. Telepon / HP</label>
              <input type="text" class="form-control" name="<?=COL_NM_PHONENO?>" value="<?=!empty($data)?$data[COL_NM_PHONENO]:""?>" />
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Password <?=$edit?'<small class="text-danger font-italic">(Kosongkan jika tidak ingin diubah)</small>':''?></h5>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label>Password <?=$edit?'Baru':''?></label>
              <input type="password" class="form-control" name="<?=COL_PASSWORD?>" />
            </div>
            <div class="form-group">
              <label>Ulangi Password <?=$edit?'Baru':''?></label>
              <input type="password" class="form-control" name="ConfirmPassword" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
<?php $this->load->view('layouts/_js') ?>
<script>
$(document).ready(function() {
  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      btnSubmit.attr('disabled', true);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('BERHASIL');
            if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
<?php $this->load->view('layouts/backend-footer') ?>
