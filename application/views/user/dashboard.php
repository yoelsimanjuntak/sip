<?php
$this->load->view('layouts/backend-header');
$ruser = GetLoggedUser();
?>
<style>
    .info-box-icon {
        height: 70px !important;
        line-height: 70px !important;
    }
    .info-box-icon-right {
        border-top-right-radius: .25rem;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: .25rem;
        display: block;
        float: right;
        height: 70px !important;
        width: 24px;
        text-align: center;
        font-size: 12px;
        line-height: 70px;
        #background: rgba(0,0,0,0.1);
    }
    .info-box-icon-right:hover {
        background: rgba(0,0,0,0.15);
    }
    .info-box-icon-right>.small-box-footer {
        color: rgba(255,255,255,0.8);
    }
    .info-box-icon-right>.small-box-footer:hover {
        color: #fff;
    }
    .info-box .info-box-content {
        padding: 3px 10px !important;
    }
</style>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
          <?php
          if($ruser[COL_ROLEID] == ROLEGURU) {
            ?>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-olive elevation-1"><i class="fas fa-book"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">TAHUN AJARAN AKTIF</span>
                    <span class="info-box-number text-lg">
                        <?=!empty($rtahunajaran)?$rtahunajaran[COL_NM_TAHUNAJARAN]:'<small class="text-danger font-italic">BELUM DIATUR</small>'?>
                    </span>
                  </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-sign"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">KELAS YANG DIAMPU</span>
                    <span class="info-box-number text-lg">
                        <?=count($rkelas)?>
                    </span>
                  </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-purple elevation-1"><i class="fas fa-book"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">MATA PELAJARAN</span>
                    <span class="info-box-number text-lg">
                        <?=count($rmapel)?>
                    </span>
                  </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-red elevation-1"><i class="fas fa-paperclip"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">MATERI DIUNGGAH</span>
                    <span class="info-box-number text-lg">
                        <?=count($rmateri)?>
                    </span>
                  </div>
                </div>
            </div>
            <?php
          } else if($ruser[COL_ROLEID] == ROLESISWA) {
            ?>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-olive elevation-1"><i class="fas fa-book"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">TAHUN AJARAN AKTIF</span>
                    <span class="info-box-number text-lg">
                        <?=!empty($rtahunajaran)?$rtahunajaran[COL_NM_TAHUNAJARAN]:'<small class="text-danger font-italic">BELUM DIATUR</small>'?>
                    </span>
                  </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-sign"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">KELAS</span>
                    <span class="info-box-number text-lg">
                        <?=!empty($rkelas)?$rkelas[COL_NM_KELAS]:'-'?>
                    </span>
                  </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-purple elevation-1"><i class="fas fa-book"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">MATA PELAJARAN</span>
                    <span class="info-box-number text-lg">
                        <?=count($rmapel)?>
                    </span>
                  </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-red elevation-1"><i class="fas fa-file"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">NILAI RATA-RATA</span>
                    <span class="info-box-number text-lg">
                      <?php
                      $text = '-';
                      $rpelajar = $this->db
                      ->where(COL_NM_NOMORINDUKSISWA, $ruser[COL_USERNAME])
                      ->get(TBL_MPELAJAR)
                      ->row_array();
                      if(!empty($rtahunajaran) && !empty($rpelajar)) {
                        $rnilai = $this->db
                        ->select('SUM((Bobot/100)*Nilai) as SUM_NILAI')
                        ->where(array(
                          COL_KD_TAHUNAJARAN=>$rtahunajaran[COL_KD_TAHUNAJARAN],
                          COL_KD_PELAJAR=>$rpelajar[COL_KD_PELAJAR]
                        ))
                        ->group_by(array(COL_KD_TAHUNAJARAN, COL_KD_MATAPELAJARAN,COL_KD_PELAJAR))
                        ->get(TBL_TNILAI)
                        ->result_array();
                        if(!empty($rnilai)) {
                          $sum_ = array_sum(array_column($rnilai, 'SUM_NILAI'));
                          $text = number_format($sum_/count($rnilai),2);
                        }
                      }
                      echo $text;
                      ?>
                    </span>
                  </div>
                </div>
            </div>
            <?php
          } else {
            ?>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-olive elevation-1"><i class="fas fa-book"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">TAHUN AJARAN AKTIF</span>
                    <span class="info-box-number text-lg">
                        <?=!empty($rtahunajaran)?$rtahunajaran[COL_NM_TAHUNAJARAN]:'<small class="text-danger font-italic">BELUM DIATUR</small>'?>
                    </span>
                  </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-sign"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">KELAS</span>
                    <span class="info-box-number text-lg">
                        <?=count($rkelas)?>
                    </span>
                  </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-purple elevation-1"><i class="fas fa-book"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">MATA PELAJARAN</span>
                    <span class="info-box-number text-lg">
                        <?=count($rmapel)?>
                    </span>
                  </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-red elevation-1"><i class="fas fa-users"></i></span>
                  <div class="info-box-content">
                    <table>
                        <tr>
                            <td>PENGAJAR</td><td class="pl-1 pr-1">:</td><td><strong><?=number_format(count($rpengajar))?></strong></td>
                        </tr>
                        <tr>
                            <td>PELAJAR</td><td class="pl-1 pr-1">:</td><td><strong><?=number_format(count($rpelajar))?></strong></td>
                        </tr>
                    </table>
                  </div>
                </div>
            </div>
            <?php
          }
          ?>
        </div>
        <div class="row">
          <?php
          if($ruser[COL_ROLEID] == ROLEGURU) {
            $arrmapel = array();
            foreach ($rmapel as $k) {
              $arrmapel[] = $k[COL_KD_MATAPELAJARAN];
            }
            $rjadwal = $this->db
            ->join(TBL_MMATAPELAJARAN,TBL_MMATAPELAJARAN.".".COL_KD_MATAPELAJARAN." = ".TBL_TKELASJADWAL.".".COL_KD_MATAPELAJARAN,"left")
            ->join(TBL_MSESI,TBL_MSESI.".".COL_KD_SESI." = ".TBL_TKELASJADWAL.".".COL_KD_SESI,"left")
            ->join(TBL_MKELAS,TBL_MKELAS.".".COL_KD_KELAS." = ".TBL_TKELASJADWAL.".".COL_KD_KELAS,"left")
            ->where(array(
              TBL_TKELASJADWAL.'.'.COL_KD_TAHUNAJARAN=>!empty($rtahunajaran)?$rtahunajaran[COL_KD_TAHUNAJARAN]:-999
            ))
            ->where_in(TBL_TKELASJADWAL.'.'.COL_KD_MATAPELAJARAN, $arrmapel)
            ->order_by(TBL_TKELASJADWAL.'.'.COL_NM_HARI, 'asc')
            ->order_by(TBL_TKELASJADWAL.'.'.COL_KD_SESI, 'asc')
            ->get(TBL_TKELASJADWAL)
            ->result_array();

            $rmateri = $this->db
            ->where(COL_CREATEDBY, $ruser[COL_USERNAME])
            ->order_by(COL_CREATEDON, 'desc')
            ->limit(10)
            ->get(TBL_TMATERI)
            ->result_array();
            ?>
            <div class="col-sm-6">
              <div class="card card-outline card-olive">
                <div class="card-header">
                  <h5 class="card-title">JADWAL</h5>
                </div>
                <div class="card-body p-0">
                  <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th>HARI</th>
                        <th>SESI</th>
                        <th>KELAS</th>
                        <th>M. PELAJARAN</th>
                      </tr>
                      <?php
                      foreach ($rjadwal as $j) {
                        ?>
                        <tr>
                          <td><?=$j[COL_NM_HARI]?></td>
                          <td><?=$j[COL_NM_SESI]?></td>
                          <td><?=$j[COL_NM_KELAS]?></td>
                          <td><?=$j[COL_NM_MATAPELAJARAN]?></td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="card card-outline card-danger">
                <div class="card-header">
                  <h5 class="card-title">MATERI DIUNGGAH</h5>
                </div>
                <div class="card-body p-0">
                  <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th>TANGGAL</th>
                        <th>JUDUL</th>
                        <th></th>
                      </tr>
                      <?php
                      foreach ($rmateri as $p) {
                        ?>
                        <tr>
                          <td style="white-space: nowrap; width: 120px;"><?=date('Y-m-d H:i', strtotime($p[COL_CREATEDON]))?></td>
                          <td style="white-space: nowrap; max-width: 120px;overflow-x: hidden; text-overflow: ellipsis"><?=$p[COL_NM_JUDUL]?></td>
                          <td class="text-center" style="white-space: nowrap; width: 10px;">
                            <a href="<?=MY_UPLOADURL.$p[COL_NM_FILE]?>" class="btn btn-xs btn-outline-primary" target="_blank"><i class="fa fa-download"></i>&nbsp;UNDUH</a>
                          </td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
                <div class="card-footer text-right">
                  <a href="<?=site_url('management/materi')?>" class="btn btn-outline-danger btn-xs">LIHAT SEMUA&nbsp;<i class="fas fa-arrow-right"></i></a>
                </div>
              </div>
            </div>
            <?php
          } else if($ruser[COL_ROLEID] == ROLESISWA) {
            $rjadwal = $this->db
            ->join(TBL_MMATAPELAJARAN,TBL_MMATAPELAJARAN.".".COL_KD_MATAPELAJARAN." = ".TBL_TKELASJADWAL.".".COL_KD_MATAPELAJARAN,"left")
            ->join(TBL_MSESI,TBL_MSESI.".".COL_KD_SESI." = ".TBL_TKELASJADWAL.".".COL_KD_SESI,"left")
            ->join(TBL_MKELAS,TBL_MKELAS.".".COL_KD_KELAS." = ".TBL_TKELASJADWAL.".".COL_KD_KELAS,"left")
            ->where(array(
              TBL_TKELASJADWAL.'.'.COL_KD_TAHUNAJARAN=>!empty($rtahunajaran)?$rtahunajaran[COL_KD_TAHUNAJARAN]:-999,
              TBL_TKELASJADWAL.'.'.COL_KD_KELAS=>!empty($rkelas)?$rkelas[COL_KD_KELAS]:-999
            ))
            ->order_by(TBL_TKELASJADWAL.'.'.COL_NM_HARI, 'asc')
            ->order_by(TBL_TKELASJADWAL.'.'.COL_KD_SESI, 'asc')
            ->get(TBL_TKELASJADWAL)
            ->result_array();

            $rmateri = $this->db
            ->where(COL_KD_KELAS, !empty($rkelas)?$rkelas[COL_KD_KELAS]:-999)
            ->order_by(COL_CREATEDON, 'desc')
            ->limit(10)
            ->get(TBL_TMATERI)
            ->result_array();
            ?>
            <div class="col-sm-6">
              <div class="card card-outline card-olive">
                <div class="card-header">
                  <h5 class="card-title">JADWAL</h5>
                </div>
                <div class="card-body p-0">
                  <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th>HARI</th>
                        <th>SESI</th>
                        <th>KELAS</th>
                        <th>M. PELAJARAN</th>
                      </tr>
                      <?php
                      foreach ($rjadwal as $j) {
                        ?>
                        <tr>
                          <td><?=$j[COL_NM_HARI]?></td>
                          <td><?=$j[COL_NM_SESI]?></td>
                          <td><?=$j[COL_NM_KELAS]?></td>
                          <td><?=$j[COL_NM_MATAPELAJARAN]?></td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="card card-outline card-danger">
                <div class="card-header">
                  <h5 class="card-title">MATERI DIUNGGAH</h5>
                </div>
                <div class="card-body p-0">
                  <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th>TANGGAL</th>
                        <th>JUDUL</th>
                        <th></th>
                      </tr>
                      <?php
                      foreach ($rmateri as $p) {
                        ?>
                        <tr>
                          <td style="white-space: nowrap; width: 120px;"><?=date('Y-m-d H:i', strtotime($p[COL_CREATEDON]))?></td>
                          <td style="white-space: nowrap; max-width: 120px;overflow-x: hidden; text-overflow: ellipsis"><?=$p[COL_NM_JUDUL]?></td>
                          <td class="text-center" style="white-space: nowrap; width: 10px;">
                            <a href="<?=MY_UPLOADURL.$p[COL_NM_FILE]?>" class="btn btn-xs btn-outline-primary" target="_blank"><i class="fa fa-download"></i>&nbsp;UNDUH</a>
                          </td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
                <div class="card-footer text-right">
                  <a href="<?=site_url('management/materi')?>" class="btn btn-outline-danger btn-xs">LIHAT SEMUA&nbsp;<i class="fas fa-arrow-right"></i></a>
                </div>
              </div>
            </div>
            <?php
          }
          ?>
        </div>
    </div>
</section>
<?php $this->load->view('layouts/_js'); ?>
<?php $this->load->view('layouts/backend-footer'); ?>
