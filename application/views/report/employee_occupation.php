<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/2/2019
 * Time: 12:58 AM
 */
$ruser = GetLoggedUser();
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        $d[COL_NM_NIK],
        $d[COL_NM_EMPLOYEE],
        $d[COL_NM_PROJECT],
        $d["Task"]
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>
<?php $this->load->view('header')
?>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $title ?> <small> Data</small></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <!--<div class="card card-outline card-default card-filter">
                        <div class="card-header">
                            <h5 class="card-title">Filter</h5>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <?=form_open(current_url(),array('role'=>'form','id'=>'filter-form','class'=>'form-horizontal', 'method'=>'get'))?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Status</label>
                                        <div class="col-sm-7">
                                            <select name="<?=COL_ID_STATUS?>" class="form-control">
                                                <option value="">-- All Status --</option>
                                                <?=GetCombobox("SELECT * FROM mstatus ORDER BY NM_Status", COL_ID_STATUS, COL_NM_STATUS, (!empty($filter[COL_ID_STATUS]) ? $filter[COL_ID_STATUS] : null))?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Category</label>
                                        <div class="col-sm-7">
                                            <select name="<?=COL_ID_CATEGORY?>" class="form-control">
                                                <option value="">-- All Category --</option>
                                                <?=GetCombobox("SELECT * FROM mcategory ORDER BY NM_Category", COL_ID_CATEGORY, COL_NM_CATEGORY, (!empty($filter[COL_ID_CATEGORY]) ? $filter[COL_ID_CATEGORY] : null))?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Customer / Owner</label>
                                        <div class="col-sm-7">
                                            <select name="<?=COL_ID_CUSTOMER?>" class="form-control">
                                                <option value="">-- All Customer / Owner --</option>
                                                <?=GetCombobox("SELECT * FROM mcustomer ORDER BY NM_Customer", COL_ID_CUSTOMER, COL_NM_CUSTOMER, (!empty($filter[COL_ID_CUSTOMER]) ? $filter[COL_ID_CUSTOMER] : null))?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Name</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="<?=COL_NM_PROJECT?>" value="<?= (!empty($filter[COL_NM_PROJECT]) ? $filter[COL_NM_PROJECT] : "")?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Sort by</label>
                                        <div class="col-sm-7">
                                            <select name="SortCol" class="form-control">
                                                <option value="<?=COL_NM_PROJECT?>" <?=(!empty($filter["SortCol"]) && $filter["SortCol"]==COL_NM_PROJECT ? "selected" : "")?>>Name</option>
                                                <option value="<?=COL_ID_CATEGORY?>" <?=(!empty($filter["SortCol"]) && $filter["SortCol"]==COL_ID_CATEGORY ? "selected" : "")?>>Category</option>
                                                <option value="<?=COL_ID_CUSTOMER?>" <?=(!empty($filter["SortCol"]) && $filter["SortCol"]==COL_ID_CUSTOMER ? "selected" : "")?>>Cutomer / Owner</option>
                                                <option value="<?=COL_ID_STATUS?>" <?=(!empty($filter["SortCol"]) && $filter["SortCol"]==COL_ID_STATUS ? "selected" : "")?>>Status</option>
                                                <option value="<?=COL_CREATEDON?>" <?=(!empty($filter["SortCol"]) ? ($filter["SortCol"]==COL_CREATEDON?"selected":"") : "selected")?>>Created On</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Sort Direction</label>
                                        <div class="col-sm-7">
                                            <select name="SortDir" class="form-control">
                                                <option value="ASC" <?=(!empty($filter["SortDir"]) && $filter["SortDir"]=='ASC' ? "selected" : "")?>>ASC</option>
                                                <option value="DESC" <?=(!empty($filter["SortDir"]) && $filter["SortDir"]=='DESC' ? "selected" : "")?>>DESC</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" style="text-align: right">
                                    <button type="submit" class="btn btn-success btn-sm">FILTER</button>
                                </div>
                            </div>
                            <?=form_close()?>
                        </div>
                    </div>-->
                    <div class="card card-default">
                        <div class="card-body">
                            <form id="dataform" method="post" action="#">
                                <table id="datalist" class="table table-bordered nowrap">

                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                "ordering": false,
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                "paging": false,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                //"scrollY" : '40vh',
                "scrollX": "120%",
                //"iDisplayLength": 100,
                //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                //"dom":"R<'row'<'col-sm-4'l><'col-sm-4'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "order": [[ 1, "asc" ]],
                "aoColumns": [
                    {"sTitle": "NIK"},
                    {"sTitle": "Employee"},
                    {"sTitle": "Project"},
                    {"sTitle": "Outstanding Task"}
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });

            $('.card-filter').find('.btn-tool').click();
        });
    </script>

<?php $this->load->view('footer')?>