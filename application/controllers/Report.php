<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 9/22/2019
 * Time: 5:42 PM
 */
class Report extends MY_Controller {
    function __construct() {
        parent::__construct();
        $ruser = GetLoggedUser();
        if(!IsLogin()) {
            redirect('user/login');
        }
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLE_PM && $ruser[COL_ROLEID] != ROLE_PMO) {
            redirect('user/dashboard');
        }
    }
}
