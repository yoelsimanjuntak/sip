<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 20/07/2019
 * Time: 12:50
 */
class Master extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/login');
        }
    }

    function profil() {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }
      $user = GetLoggedUser();
      $data['data'] = $rdata = $this->db->order_by(COL_UNIQ, 'desc')->get(TBL_MPROFIL)->row_array();
      $data['title'] = "Profil Institusi";
      $data['edit'] = TRUE;

      if(!empty($_POST)) {
        $data['data'] = $_POST;
        $rec = array(
          COL_NM_INSTITUSI => $this->input->post(COL_NM_INSTITUSI),
          COL_NM_ALAMAT => $this->input->post(COL_NM_ALAMAT),
          COL_NM_PHONENO => $this->input->post(COL_NM_PHONENO),
          COL_NM_EMAIL => $this->input->post(COL_NM_EMAIL),
          COL_NM_BIO => $this->input->post(COL_NM_BIO),

          COL_UPDATEDBY => $user[COL_USERNAME],
          COL_UPDATEDON => date('Y-m-d H:i:s')
        );
        $this->db->trans_begin();

        try {
          if(!empty($rdata)) {
            $res = $this->db->update(TBL_MPROFIL, $rec);
          } else {
            $res = $this->db->insert(TBL_MPROFIL, $rec);
          }

          if($res) {
            $this->db->trans_commit();
            redirect('master/profil');
          } else {
            $this->db->trans_rollback();
            redirect(current_url()."?error=1");
          }
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          redirect(current_url()."?error=1");
        }
      } else {
        $this->load->view('master/form_profil', $data);
      }
    }

    function period_index() {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        redirect('user/dashboard');
      }

      $data['title'] = "Periode Kepemimpinan";
      $data['res'] = $this->db->get(TBL_MPERIODE)->result_array();
      $this->load->view('master/index_period', $data);
    }

    function period_add() {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }
      $user = GetLoggedUser();
      $data['title'] = "Periode Kepemimpinan";
      $data['edit'] = FALSE;
      if(!empty($_POST)){
        $data['data'] = $_POST;
        $rec = array(
            COL_NM_PIMPINAN => $this->input->post(COL_NM_PIMPINAN),
            COL_NM_WAKILPIMPINAN => $this->input->post(COL_NM_WAKILPIMPINAN),
            COL_TAHUNFROM => $this->input->post(COL_TAHUNFROM),
            COL_TAHUNTO => $this->input->post(COL_TAHUNTO),

            COL_CREATEDBY => $user[COL_USERNAME],
            COL_CREATEDON => date('Y-m-d H:i:s')
        );
        $this->db->trans_begin();
        try {
          $res = $this->db->insert(TBL_MPERIODE, $rec);
          if($res) {
            $this->db->trans_commit();
            redirect('master/period-index');
          } else {
            $this->db->trans_rollback();
            redirect(current_url()."?error=1");
          }
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          redirect(current_url()."?error=1");
        }
      }
      else {
        $this->load->view('master/form_period', $data);
      }
    }

    function period_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
        }
        $user = GetLoggedUser();
        $rdata = $data['data'] = $this->db->where(COL_KD_PERIODE, $id)->get(TBL_MPERIODE)->row_array();
        if(empty($rdata)){
          show_404();
          return;
        }

        $data['title'] = "Periode Kepemimpinan";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
          $data['data'] = $_POST;
          $rec = array(
            COL_NM_PIMPINAN => $this->input->post(COL_NM_PIMPINAN),
            COL_NM_WAKILPIMPINAN => $this->input->post(COL_NM_WAKILPIMPINAN),
            COL_TAHUNFROM => $this->input->post(COL_TAHUNFROM),
            COL_TAHUNTO => $this->input->post(COL_TAHUNTO),

            COL_UPDATEDBY => $user[COL_USERNAME],
            COL_UPDATEDON => date('Y-m-d H:i:s')
          );
          $this->db->trans_begin();
          try {
            $res = $this->db->where(COL_KD_PERIODE, $id)->update(TBL_MPERIODE, $rec);
            if($res) {
                $this->db->trans_commit();
                redirect('master/period-index');
            } else {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
          } catch(Exception $ex) {
            $this->db->trans_rollback();
            redirect(current_url()."?error=1");
          }
        }
        else {
            $this->load->view('master/form_period', $data);
        }
    }

    function period_delete(){
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          ShowJsonError("Not Authorized.");
          return;
      }
      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
        $this->db->delete(TBL_MPERIODE, array(COL_KD_PERIODE => $datum));
        $deleted++;
      }
      if($deleted) {
        ShowJsonSuccess($deleted." data dihapus");
      } else {
        ShowJsonError("Tidak ada dihapus");
      }
    }

    function tahun_ajar_index() {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }

      $data['title'] = "Tahun Ajaran";
      $data['res'] = $this->db->get(TBL_MTAHUNAJARAN)->result_array();
      $this->load->view('master/index_tahunajaran', $data);
    }

    function tahun_ajar_add() {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }
      $user = GetLoggedUser();
      if(!empty($_POST)){
        $data['data'] = $_POST;
        $data = array(
            COL_NM_TAHUNAJARAN => $this->input->post(COL_NM_TAHUNAJARAN),
            COL_IS_ACTIVE => $this->input->post(COL_IS_ACTIVE)&&$this->input->post(COL_IS_ACTIVE)=='on',

            COL_CREATEDBY => $user[COL_USERNAME],
            COL_CREATEDON => date('Y-m-d H:i:s')
        );

        $res = $this->db->insert(TBL_MTAHUNAJARAN, $data);
        if($res) {
          ShowJsonSuccess("Berhasil");
        } else {
          ShowJsonError("Gagal");
        }
      }
    }

    function tahun_ajar_edit($id) {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }
      $user = GetLoggedUser();
      if(!empty($_POST)){
        $data['data'] = $_POST;
        $data = array(
          COL_NM_TAHUNAJARAN => $this->input->post(COL_NM_TAHUNAJARAN),
          COL_IS_ACTIVE => $this->input->post(COL_IS_ACTIVE)&&$this->input->post(COL_IS_ACTIVE)=='on',

          COL_UPDATEDBY => $user[COL_USERNAME],
          COL_UPDATEDON => date('Y-m-d H:i:s')
        );

        $res = $this->db->where(COL_KD_TAHUNAJARAN, $id)->update(TBL_MTAHUNAJARAN, $data);
        if($res) {
          ShowJsonSuccess("Berhasil");
        } else {
          ShowJsonError("Gagal");
        }
      }
    }

    function tahun_ajar_delete(){
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError("Not Authorized.");
        return;
      }
      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
        $this->db->delete(TBL_MTAHUNAJARAN, array(COL_KD_TAHUNAJARAN => $datum));
        $deleted++;
      }
      if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
      }else{
        ShowJsonError("Tidak ada dihapus");
      }
    }

    function kelas_index() {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }

      $data['title'] = "Kelas";
      $data['res'] = $this->db->get(TBL_MKELAS)->result_array();
      $this->load->view('master/index_kelas', $data);
    }

    function kelas_add() {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }
      $user = GetLoggedUser();
      if(!empty($_POST)){
        $data['data'] = $_POST;
        $data = array(
            COL_NM_KELAS => $this->input->post(COL_NM_KELAS),

            COL_CREATEDBY => $user[COL_USERNAME],
            COL_CREATEDON => date('Y-m-d H:i:s')
        );

        $res = $this->db->insert(TBL_MKELAS, $data);
        if($res) {
          ShowJsonSuccess("Berhasil");
        } else {
          ShowJsonError("Gagal");
        }
      }
    }

    function kelas_edit($id) {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }
      $user = GetLoggedUser();
      if(!empty($_POST)){
        $data['data'] = $_POST;
        $data = array(
          COL_NM_KELAS => $this->input->post(COL_NM_KELAS),

          COL_UPDATEDBY => $user[COL_USERNAME],
          COL_UPDATEDON => date('Y-m-d H:i:s')
        );

        $res = $this->db->where(COL_KD_KELAS, $id)->update(TBL_MKELAS, $data);
        if($res) {
          ShowJsonSuccess("Berhasil");
        } else {
          ShowJsonError("Gagal");
        }
      }
    }

    function kelas_delete(){
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError("Not Authorized.");
        return;
      }
      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
        $this->db->delete(TBL_MKELAS, array(COL_KD_KELAS => $datum));
        $deleted++;
      }
      if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
      }else{
        ShowJsonError("Tidak ada dihapus");
      }
    }

    function sesi_index() {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }

      $data['title'] = "Sesi Belajar - Mengajar";
      $data['res'] = $this->db->get(TBL_MSESI)->result_array();
      $this->load->view('master/index_sesi', $data);
    }

    function sesi_add() {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }
      $user = GetLoggedUser();
      if(!empty($_POST)){
        $data['data'] = $_POST;
        $data = array(
            COL_NM_SESI => $this->input->post(COL_NM_SESI),
            COL_JAM_FROM => $this->input->post(COL_JAM_FROM),
            COL_JAM_TO => $this->input->post(COL_JAM_TO),

            COL_CREATEDBY => $user[COL_USERNAME],
            COL_CREATEDON => date('Y-m-d H:i:s')
        );

        $res = $this->db->insert(TBL_MSESI, $data);
        if($res) {
          ShowJsonSuccess("Berhasil");
        } else {
          ShowJsonError("Gagal");
        }
      }
    }

    function sesi_edit($id) {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }
      $user = GetLoggedUser();
      if(!empty($_POST)){
        $data['data'] = $_POST;
        $data = array(
          COL_NM_SESI => $this->input->post(COL_NM_SESI),
          COL_JAM_FROM => $this->input->post(COL_JAM_FROM),
          COL_JAM_TO => $this->input->post(COL_JAM_TO),

          COL_UPDATEDBY => $user[COL_USERNAME],
          COL_UPDATEDON => date('Y-m-d H:i:s')
        );

        $res = $this->db->where(COL_KD_SESI, $id)->update(TBL_MSESI, $data);
        if($res) {
          ShowJsonSuccess("Berhasil");
        } else {
          ShowJsonError("Gagal");
        }
      }
    }

    function sesi_delete(){
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError("Not Authorized.");
        return;
      }
      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
        $this->db->delete(TBL_MSESI, array(COL_KD_SESI => $datum));
        $deleted++;
      }
      if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
      }else{
        ShowJsonError("Tidak ada dihapus");
      }
    }

    function subject_index() {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }

      $data['title'] = "Mata Pelajaran";
      $data['res'] = $this->db->get(TBL_MMATAPELAJARAN)->result_array();
      $this->load->view('master/index_subject', $data);
    }

    function subject_add() {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }
      $user = GetLoggedUser();
      if(!empty($_POST)){
        $data['data'] = $_POST;
        $data = array(
            COL_NM_MATAPELAJARAN => $this->input->post(COL_NM_MATAPELAJARAN),

            COL_CREATEDBY => $user[COL_USERNAME],
            COL_CREATEDON => date('Y-m-d H:i:s')
        );

        $res = $this->db->insert(TBL_MMATAPELAJARAN, $data);
        if($res) {
          ShowJsonSuccess("Berhasil");
        } else {
          ShowJsonError("Gagal");
        }
      }
    }

    function subject_edit($id) {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }
      $user = GetLoggedUser();
      if(!empty($_POST)){
        $data['data'] = $_POST;
        $data = array(
          COL_NM_MATAPELAJARAN => $this->input->post(COL_NM_MATAPELAJARAN),

          COL_UPDATEDBY => $user[COL_USERNAME],
          COL_UPDATEDON => date('Y-m-d H:i:s')
        );

        $res = $this->db->where(COL_KD_MATAPELAJARAN, $id)->update(TBL_MMATAPELAJARAN, $data);
        if($res) {
          ShowJsonSuccess("Berhasil");
        } else {
          ShowJsonError("Gagal");
        }
      }
    }

    function subject_delete(){
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError("Not Authorized.");
        return;
      }
      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
        $this->db->delete(TBL_MMATAPELAJARAN, array(COL_KD_MATAPELAJARAN => $datum));
        $deleted++;
      }
      if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
      }else{
        ShowJsonError("Tidak ada dihapus");
      }
    }

    function test() {
      $this->load->view('master/index_test');
    }
}
