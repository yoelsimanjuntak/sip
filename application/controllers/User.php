<?php
class User extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->helper('captcha');
        $this->load->library('user_agent');
        $this->load->model('muser');
    }

    function index($role) {
        if(!IsLogin()) {
            redirect('user/login');
        }
        $loginuser = GetLoggedUser();
        if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
            show_error('Anda tidak memiliki akses terhadap modul ini.');
            return;
        }

        $data['title'] = "Pengguna";
        $data['role'] = $role;
        if($role==ROLEGURU) {
          $this->db->select('*, ref.Nm_Pengajar as Nm_FullName');
          $this->db->join(TBL_MPENGAJAR.' ref','ref.'.COL_NM_NOMORINDUKPEGAWAI." = ".TBL_USERS.".".COL_USERNAME,"left");
        }
        else if($role==ROLESISWA) {
          $this->db->select('*, ref.Nm_Pelajar as Nm_FullName');
          $this->db->join(TBL_MPELAJAR.' ref','ref.'.COL_NM_NOMORINDUKSISWA." = ".TBL_USERS.".".COL_USERNAME,"left");
        }
        $this->db->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = ".TBL_USERS.".".COL_USERNAME,"inner");
        $this->db->join(TBL_ROLES,TBL_ROLES.'.'.COL_ROLEID." = ".TBL_USERS.".".COL_ROLEID,"inner");
        $this->db->where(TBL_USERS.".".COL_ROLEID,$role);
        $this->db->order_by(TBL_USERINFORMATION.".".COL_NM_FULLNAME, 'asc');
        $data['res'] = $this->db->get(TBL_USERS)->result_array();
        $this->load->view('user/index', $data);
    }

    function Login(){
        if(IsLogin()) {
            redirect('user/dashboard');
        }
        $rules = array(
            array(
                'field' => 'UserName',
                'label' => 'UserName',
                'rules' => 'required'
            ),
            array(
                'field' => 'Password',
                'label' => 'Password',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()){
            //$this->load->library('si/securimage');
            $username = $this->input->post(COL_USERNAME);
            $password = $this->input->post(COL_PASSWORD);

            /*if($this->securimage->check($this->input->post('Captcha')) != true){
                redirect(site_url('user/login')."?msg=captcha");
            }*/

            if($this->muser->authenticate($username, $password)) {
                if($this->muser->IsSuspend($username)) {
                    redirect(site_url('user/login')."?msg=suspend");
                }

                // Update Last Login IP
                $this->db->where(COL_USERNAME, $username);
                $this->db->update(TBL_USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));

                $userdetails = $this->muser->getdetails($username);
                SetLoginSession($userdetails);
                if($userdetails[COL_ROLEID]==ROLEADMIN) {
                  redirect('user/dashboard');
                } else {
                  redirect('home/index');
                }

            }
            else {
                redirect(site_url('user/login')."?msg=notmatch");
            }
        } else {
            $this->load->view('user/login');
        }

    }
    function Logout(){
        UnsetLoginSession();
        redirect(site_url());
    }
    function Dashboard() {
        if(!IsLogin()) {
            redirect('user/login');
        }
        $ruser = GetLoggedUser();

        $data['title'] = 'Dashboard';
        $data['rtahunajaran'] = $rTahunAjaran = $this->db
        ->where(COL_IS_ACTIVE, 1)
        ->order_by(COL_NM_TAHUNAJARAN, 'desc')
        ->get(TBL_MTAHUNAJARAN)
        ->row_array();

        if($ruser[COL_ROLEID] == ROLEGURU) {
          $data['rkelas'] = array();
          $data['rmapel'] = array();
          $data['rmateri'] = array();

          $mpengajar = $this->db
          ->where(COL_NM_NOMORINDUKPEGAWAI, $ruser[COL_USERNAME])
          ->get(TBL_MPENGAJAR)
          ->row_array();

          if(!empty($mpengajar) && !empty($rTahunAjaran)) {
            $data['rkelas'] = $this->db
            ->where(array(
              COL_KD_TAHUNAJARAN=>$rTahunAjaran[COL_KD_TAHUNAJARAN],
              COL_KD_PENGAJAR=>$mpengajar[COL_KD_PENGAJAR]
            ))
            ->group_by(COL_KD_KELAS)
            ->get(TBL_TKELASMATAPELAJARAN)
            ->result_array();

            $data['rmapel'] = $this->db
            ->where(array(
              COL_KD_TAHUNAJARAN=>$rTahunAjaran[COL_KD_TAHUNAJARAN],
              COL_KD_PENGAJAR=>$mpengajar[COL_KD_PENGAJAR]
            ))
            ->group_by(COL_KD_MATAPELAJARAN)
            ->get(TBL_TKELASMATAPELAJARAN)
            ->result_array();

            $data['rmateri'] = $this->db
            ->where(array(
              COL_KD_TAHUNAJARAN=>$rTahunAjaran[COL_KD_TAHUNAJARAN],
              COL_CREATEDBY=>$mpengajar[COL_NM_NOMORINDUKPEGAWAI]
            ))
            ->get(TBL_TMATERI)
            ->result_array();
          }
        } else if($ruser[COL_ROLEID] == ROLESISWA) {
          $data['rkelas'] = array();
          $data['rmapel'] = array();
          $data['rmateri'] = array();

          $mpelajar = $this->db
          ->where(COL_NM_NOMORINDUKSISWA, $ruser[COL_USERNAME])
          ->get(TBL_MPELAJAR)
          ->row_array();

          $data['rkelas'] = $rkelas = $this->db
          ->join(TBL_MKELAS,TBL_MKELAS.'.'.COL_KD_KELAS." = ".TBL_TKELASPELAJAR.".".COL_KD_KELAS,"inner")
          ->where(array(
            COL_KD_TAHUNAJARAN=>$rTahunAjaran[COL_KD_TAHUNAJARAN],
            COL_KD_PELAJAR=>$mpelajar[COL_KD_PELAJAR]
          ))
          ->get(TBL_TKELASPELAJAR)
          ->row_array();

          if(!empty($rkelas)) {
            $data['rmapel'] = $this->db
            ->where(array(
              COL_KD_TAHUNAJARAN=>$rTahunAjaran[COL_KD_TAHUNAJARAN],
              COL_KD_KELAS=>$rkelas[COL_KD_KELAS]
            ))
            ->group_by(COL_KD_MATAPELAJARAN)
            ->get(TBL_TKELASMATAPELAJARAN)
            ->result_array();
          }

        } else {
          $data['rkelas'] = $this->db
          ->get(TBL_MKELAS)
          ->result_array();

          $data['rmapel'] = $this->db
          ->get(TBL_MMATAPELAJARAN)
          ->result_array();

          $data['rpengajar'] = $this->db
          ->get(TBL_MPENGAJAR)
          ->result_array();

          $data['rpelajar'] = $this->db
          ->get(TBL_MPELAJAR)
          ->result_array();

        }

        $this->load->view('user/dashboard', $data);
    }
    function ChangePassword() {
        if(!IsLogin()) {
            redirect('user/login');
        }
        $user = GetLoggedUser();
        $data['title'] = 'Change Password';
        $rules = array(
            array(
                'field' => 'OldPassword',
                'label' => 'Old Password',
                'rules' => 'required'
            ),
            array(
                'field' => COL_PASSWORD,
                'label' => COL_PASSWORD,
                'rules' => 'required'
            ),
            array(
                'field' => 'RepeatPassword',
                'label' => 'Repeat Password',
                'rules' => 'required|matches[Password]'
            )
        );
        $this->form_validation->set_rules($rules);

        if($this->form_validation->run()){
            $rcheck = $this->db->where(COL_USERNAME, $user[COL_USERNAME])->get(TBL_USERS)->row_array();
            if(!$rcheck) {
                redirect(site_url('user/changepassword'));
            }
            if($rcheck[COL_PASSWORD] != md5($this->input->post("OldPassword"))) {
                redirect(site_url('user/changepassword')."?nomatch=1");
            }
            $upd = $this->db->where(COL_USERNAME, $user[COL_USERNAME])->update(TBL_USERS, array(COL_PASSWORD=>md5($this->input->post(COL_PASSWORD))));
            if($upd) redirect(site_url('user/changepassword')."?success=1");
            else redirect(site_url('user/changepassword')."?error=1");
        }
        else {
            $this->load->view('user/changepassword', $data);
        }
    }

    function delete(){
        if(!IsLogin()) {
            ShowJsonError('Silahkan login terlebih dahulu');
            return;
        }
        $loginuser = GetLoggedUser();
        if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError('Anda tidak memiliki akses terhadap modul ini.');
            return;
        }
        $this->load->model('muser');
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            if($this->muser->delete($datum)) {
                $deleted++;
            }
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada data dihapus");
        }
    }

    function activate($suspend=false) {
      if(!IsLogin()) {
        ShowJsonError('Silahkan login terlebih dahulu');
        return;
      }
      $loginuser = GetLoggedUser();
      if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError('Anda tidak memiliki akses terhadap modul ini.');
        return;
      }
      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
        if($this->db->where(COL_USERNAME, $datum)->update(TBL_USERS, array(COL_ISSUSPEND=>$suspend))) {
          $deleted++;
        }
        if(!$suspend) {
          $this->db->where(COL_USERNAME, $datum)->update(TBL_USERINFORMATION, array(COL_REGISTEREDDATE=>date('Y-m-d H:i:s')));
        }
      }
      if($deleted){
        ShowJsonSuccess($deleted." data diubah");
      }else{
        ShowJsonError("Tidak ada data yang diubah");
      }
    }

    function add($role) {
      if(!IsLogin()) {
        redirect(site_url('user/login'));
      }
      $loginuser = GetLoggedUser();
      if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
        show_error('Anda tidak memiliki akses terhadap modul ini.');
        return;
      }

      $data['title'] = "Pengguna";
      $data['edit'] = FALSE;
      $data['role'] = $role;
      if(!empty($_POST)){
        $data['data'] = $_POST;
        $rcheck = $this->db
        ->where(COL_USERNAME, $this->input->post(COL_USERNAME))
        ->get(TBL_USERS)
        ->row_array();
        if(!empty($rcheck)) {
          ShowJsonError('Akun sudah terdaftar di sistem');
          return false;
        }

        if($this->input->post(COL_PASSWORD) != $this->input->post("ConfirmPassword")) {
          ShowJsonError('Harap ulangi password dengan benar');
          return false;
        }

        $userdata = array(
          COL_USERNAME => $this->input->post(COL_USERNAME),
          COL_PASSWORD => md5($this->input->post(COL_PASSWORD)),
          COL_ROLEID => $role,
          COL_ISSUSPEND => false
        );
        $userinfo = array(
          COL_USERNAME => $this->input->post(COL_USERNAME),
          COL_NM_EMAIL => $this->input->post(COL_NM_EMAIL),
          COL_NM_PHONENO => $this->input->post(COL_NM_PHONENO),
          COL_DATE_REGISTERED => date('Y-m-d')
        );

        $this->db->trans_begin();
        try {
          $res = $this->db->insert(TBL_USERS, $userdata);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
          $res = $this->db->insert(TBL_USERINFORMATION, $userinfo);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          return false;
        }

        $this->db->trans_commit();
        ShowJsonSuccess('BERHASIL', array('redirect'=>site_url('user/index/'.$role)));
        return true;
      }
      else {
        $this->load->view('user/form', $data);
      }
    }

    function edit($id) {
        if(!IsLogin()) {
            redirect(site_url('user/login'));
        }
        $loginuser = GetLoggedUser();
        if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
          show_error('Anda tidak memiliki akses terhadap modul ini.');
          return;
        }
        $data['title'] = "Pengguna";
        $data['edit'] = TRUE;

        $data['data'] = $edited = $this->muser->getdetails($id);
        if(empty($edited)){
          show_404();
          return;
        }

        $data['role'] = $role = $edited[COL_ROLEID];
        if(!empty($_POST)){
          $rcheck = $this->db
          ->where(COL_USERNAME, $this->input->post(COL_USERNAME))
          ->get(TBL_USERS)
          ->row_array();
          if(!empty($rcheck)) {
            ShowJsonError('Akun sudah terdaftar di sistem');
            return false;
          }

          $userdata = array();
          if(!empty($this->input->post(COL_PASSWORD))) {
            if($this->input->post(COL_PASSWORD) != $this->input->post("ConfirmPassword")) {
              ShowJsonError('Harap ulangi password dengan benar');
              return false;
            }
            $userdata[COL_PASSWORD] = md5($this->input->post(COL_PASSWORD));
          }
          $userinfo = array(
            COL_NM_EMAIL => $this->input->post(COL_NM_EMAIL),
            COL_NM_PHONENO => $this->input->post(COL_NM_PHONENO)
          );

          $this->db->trans_begin();
          try {
            if(!empty($userdata)) {
              $res = $this->db->where(COL_USERNAME, $edited[COL_USERNAME])->update(TBL_USERS, $userdata);
              if(!$res) {
                $err = $this->db->error();
                throw new Exception($err['message']);
              }
            }
            $res = $this->db->where(COL_USERNAME, $edited[COL_USERNAME])->update(TBL_USERINFORMATION, $userinfo);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          } catch(Exception $ex) {
            $this->db->trans_rollback();
            ShowJsonError($ex->getMessage());
            return false;
          }

          $this->db->trans_commit();
          ShowJsonSuccess('BERHASIL', array('redirect'=>site_url('user/index/'.$role)));
          return true;
        }
        else {
          $this->load->view('user/form', $data);
        }
    }

    function detail($uname) {
        $username = GetDecryption($uname);

        $data['data'] = $rdata = $this->muser->getdetails($username);
        if(!$rdata) {
            show_404();
            return false;
        }
        $data['title'] = $rdata[COL_NAME];
        $this->load->view('user/detail', $data);
    }

    function encrypt() {
        echo GetEncryption("yoelrolas");
    }

    function Preference() {
        if(!IsLogin()) {
            redirect('user/login');
        }
        $loginuser = GetLoggedUser();
        if(!$loginuser || $loginuser[COL_ROLEID] != ROLEUSER) {
            show_error('Anda tidak memiliki akses terhadap modul ini.');
            return;
        }

        $data["title"] = "Preference";
        if(!empty($_POST)) {
            $this->db->trans_begin();
            // Delete preference of this user
            if(!$this->db->where(COL_USERNAME, $loginuser[COL_USERNAME])->delete(TBL_USERPREFERENCES)) {
                $this->db->trans_rollback();
                echo "gagal delete";
                return;
                redirect(current_url());
            }

            $pref_industry = $this->input->post(COL_PREFERENCEVALUE."-".PREFERENCETYPE_INDUSTRYTYPE);
            if($pref_industry && count($pref_industry) > 0) {
                $arrinsert = array();
                for($i=0; $i<count($pref_industry); $i++) {
                    $p = $pref_industry[$i];
                    $arrinsert[] = array(
                        COL_USERNAME => $loginuser[COL_USERNAME],
                        COL_PREFERENCETYPEID => PREFERENCETYPE_INDUSTRYTYPE,
                        COL_PREFERENCEVALUE => $p
                    );
                }
                if(!$this->db->insert_batch(TBL_USERPREFERENCES, $arrinsert)) {
                    $this->db->trans_rollback();
                    redirect(current_url());
                }
            }

            $pref_education = $this->input->post(COL_PREFERENCEVALUE."-".PREFERENCETYPE_EDUCATIONTYPE);
            if($pref_education && count($pref_education) > 0) {
                $arrinsert = array();
                for($i=0; $i<count($pref_education); $i++) {
                    $p = $pref_education[$i];
                    $arrinsert[] = array(
                        COL_USERNAME => $loginuser[COL_USERNAME],
                        COL_PREFERENCETYPEID => PREFERENCETYPE_EDUCATIONTYPE,
                        COL_PREFERENCEVALUE => $p
                    );
                }
                if(!$this->db->insert_batch(TBL_USERPREFERENCES, $arrinsert)) {
                    $this->db->trans_rollback();
                    redirect(current_url());
                }
            }

            $pref_position = $this->input->post(COL_PREFERENCEVALUE."-".PREFERENCETYPE_POSITION);
            if($pref_position && count($pref_position) > 0) {
                $arrinsert = array();
                for($i=0; $i<count($pref_position); $i++) {
                    $p = $pref_position[$i];
                    $arrinsert[] = array(
                        COL_USERNAME => $loginuser[COL_USERNAME],
                        COL_PREFERENCETYPEID => PREFERENCETYPE_POSITION,
                        COL_PREFERENCEVALUE => $p
                    );
                }
                if(!$this->db->insert_batch(TBL_USERPREFERENCES, $arrinsert)) {
                    $this->db->trans_rollback();
                    redirect(current_url());
                }
            }

            $pref_location = $this->input->post(COL_PREFERENCEVALUE."-".PREFERENCETYPE_LOCATION);
            if($pref_location && count($pref_location) > 0) {
                $arrinsert = array();
                for($i=0; $i<count($pref_location); $i++) {
                    $p = $pref_location[$i];
                    $arrinsert[] = array(
                        COL_USERNAME => $loginuser[COL_USERNAME],
                        COL_PREFERENCETYPEID => PREFERENCETYPE_LOCATION,
                        COL_PREFERENCEVALUE => $p
                    );
                }
                if(!$this->db->insert_batch(TBL_USERPREFERENCES, $arrinsert)) {
                    $this->db->trans_rollback();
                    redirect(current_url());
                }
            }

            $pref_type = $this->input->post(COL_PREFERENCEVALUE."-".PREFERENCETYPE_VACANCYTYPE);
            if($pref_type && count($pref_type) > 0) {
                $arrinsert = array();
                for($i=0; $i<count($pref_type); $i++) {
                    $p = $pref_type[$i];
                    $arrinsert[] = array(
                        COL_USERNAME => $loginuser[COL_USERNAME],
                        COL_PREFERENCETYPEID => PREFERENCETYPE_VACANCYTYPE,
                        COL_PREFERENCEVALUE => $p
                    );
                }
                if(!$this->db->insert_batch(TBL_USERPREFERENCES, $arrinsert)) {
                    $this->db->trans_rollback();
                    redirect(current_url());
                }
            }
            $this->db->trans_commit();
        }

        $pref_industry = array();
        $pref_education = array();
        $pref_position = array();
        $pref_location = array();
        $pref_type = array();
        $data["pref"] = $pref = $this->db->where(COL_USERNAME, $loginuser[COL_USERNAME])->get(TBL_USERPREFERENCES)->result_array();

        if($pref && count($pref) > 0) {
            foreach($pref as $p) {
                if($p[COL_PREFERENCETYPEID] == PREFERENCETYPE_INDUSTRYTYPE) $pref_industry[] = $p[COL_PREFERENCEVALUE];
                if($p[COL_PREFERENCETYPEID] == PREFERENCETYPE_EDUCATIONTYPE) $pref_education[] = $p[COL_PREFERENCEVALUE];
                if($p[COL_PREFERENCETYPEID] == PREFERENCETYPE_POSITION) $pref_position[] = $p[COL_PREFERENCEVALUE];
                if($p[COL_PREFERENCETYPEID] == PREFERENCETYPE_LOCATION) $pref_location[] = $p[COL_PREFERENCEVALUE];
                if($p[COL_PREFERENCETYPEID] == PREFERENCETYPE_VACANCYTYPE) $pref_type[] = $p[COL_PREFERENCEVALUE];
            }
        }
        $data["industry"] = $pref_industry;
        $data["education"] = $pref_education;
        $data["position"] = $pref_position;
        $data["location"] = $pref_location;
        $data["type"] = $pref_type;

        $this->load->view('user/preference', $data);
    }

    function register() {
        $data['title'] = "Daftar";
        $rules = $arr = array(
            array(
                'field' => COL_NM_EMAIL,
                'label' => COL_NM_EMAIL,
                'rules' => 'required|valid_email|is_unique[userinformation.NM_Email]',
                'errors' => array('is_unique' => 'Email already registered on system')
            ),
            array(
                'field' => COL_PASSWORD,
                'label' => COL_PASSWORD,
                'rules' => 'required|min_length[5]'
            ),
            array(
                'field' => 'RepeatPassword',
                'label' => 'Repeat Password',
                'rules' => 'required|matches[Password]'
            )
        );
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()){
            $data['data'] = $_POST;

            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 500;
            $config['max_width']  = 1024;
            $config['max_height']  = 768;
            $config['overwrite'] = TRUE;
            if(!empty($_FILES["avatar"]["name"])) {
              $email = $this->input->post(COL_NM_EMAIL);
              $email_split = explode('@', $email);
              if(!empty($email_split)) {
                $config['file_name'] = strtolower(reset($email_split)).".".end((explode(".", $_FILES["avatar"]["name"])));
              }

              $this->load->library('upload',$config);
              if(!$this->upload->do_upload("avatar")){
                  $data['upload_errors'] = $this->upload->display_errors();
                  $this->load->view('user/register', $data);
                  return;
              }
            }

            $dataupload = $this->upload->data();

            $this->db->trans_begin();
            try {
                $userdata = array(
                    COL_USERNAME => $this->input->post(COL_NM_EMAIL),
                    COL_PASSWORD => md5($this->input->post(COL_PASSWORD)),
                    COL_ROLEID => $this->input->post(COL_ROLEID),
                    COL_ISSUSPEND => false
                );
                $userinfo = array(
                    COL_USERNAME => $this->input->post(COL_NM_EMAIL),
                    COL_NM_EMAIL => $this->input->post(COL_NM_EMAIL),
                    COL_NM_FULLNAME => $this->input->post(COL_NM_FULLNAME),
                    COL_NM_GENDER => $this->input->post(COL_NM_GENDER),
                    COL_DATE_BIRTH => date('Y-m-d', strtotime($this->input->post(COL_DATE_BIRTH))),
                    COL_NM_ADDRESS => $this->input->post(COL_NM_ADDRESS),
                    COL_NM_PHONENO => $this->input->post(COL_NM_PHONENO),
                    COL_NM_ABOUT => $this->input->post(COL_NM_ABOUT),
                    COL_DATE_REGISTERED => date('Y-m-d')
                );
                if(!empty($dataupload) && $dataupload['file_name']) {
                    $userinfo[COL_NM_IMAGELOCATION] = $dataupload['file_name'];
                }

                $reg = $this->muser->register($userdata, $userinfo, null);
                if($reg) {
                    $this->db->trans_commit();
                    redirect(site_url('home/index'));
                }
                else {
                    $this->db->trans_rollback();
                    redirect(current_url().'?error=1');
                }
            } catch(Exception $ex) {
                $this->db->trans_rollback();
                redirect(current_url().'?error=1');
            }
        } else{
            $this->load->view('user/register', $data);
        }
    }

    function ForgotPassword() {
        $data["title"] = "Lupa Password";
        if(!empty($_POST)) {
            $email = $this->input->post(COL_EMAIL);

            $this->db->where(COL_EMAIL, $email);
            $ruser = $this->db->get(TBL_USERINFORMATION)->row_array();
            if(!$ruser) {
                redirect(current_url()."?notfound=1");
                return false;
            }

            $encryptedmail= GetEncryption($ruser[COL_EMAIL]);
            if(IsNotificationActive(NOTIFICATION_LUPAPASSWORD)) {
                $this->load->library('email',GetEmailConfig());
                $this->email->set_newline("\r\n");

                $pref = GetNotification(NOTIFICATION_LUPAPASSWORD);

                $content = $pref[COL_NOTIFICATIONCONTENT];
                $subject = $pref[COL_NOTIFICATIONSUBJECT];

                $subject = str_replace(array("@SITENAME@"), array(SITENAME), $subject);
                $content = str_replace(array("@NAME@", "@RECOVERYLINK@", "@SITENAME@"),
                    array((!empty($ruser[COL_NAME])?$ruser[COL_NAME]:"Unknown"), site_url("user/resetpassword")."?token=".$encryptedmail, SITENAME),
                    $content);

                /*$this->email->from($pref[COL_NOTIFICATIONSENDEREMAIL], $pref[COL_NOTIFICATIONSENDERNAME]);
                $this->email->to($ruser[COL_EMAIL]);
                $this->email->subject($subject);
                $this->email->message($content);
                $this->email->send();*/

                $headers = 'From: '.$pref[COL_NOTIFICATIONSENDERNAME].' <'.$pref[COL_NOTIFICATIONSENDEREMAIL].'>' . PHP_EOL .
                    'Reply-To: '.$pref[COL_NOTIFICATIONSENDEREMAIL].' <'.$pref[COL_NOTIFICATIONSENDEREMAIL].'>' . PHP_EOL .
                    'Return-Path: '.$pref[COL_NOTIFICATIONSENDEREMAIL].' <'.$pref[COL_NOTIFICATIONSENDEREMAIL].'>' . PHP_EOL .
                    'MIME-Version: 1.0' . PHP_EOL .
                    'Content-Type: text/html; charset=ISO-8859-1' . PHP_EOL;
                mail($ruser[COL_EMAIL], $subject, str_replace("\n.", "\n..", $content), $headers);
            }

            redirect(current_url()."?success=1");
            return true;
        } else {
            $this->load->view("user/forgotpassword", $data);
            return true;
        }
    }

    function ResetPassword() {
        $data["title"] = "Reset Password";
        $data["token"] = $token = $this->input->get("token");
        if(!$token) {
            show_404();
            return false;
        }

        $email = GetDecryption($token);
        $this->db->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_USERINFORMATION.".".COL_USERNAME,"inner");
        $this->db->where(COL_EMAIL, $email);
        $ruser = $this->db->get(TBL_USERINFORMATION)->row_array();
        if(!$ruser) {
            show_404();
            return false;
        }

        $rules = array(
            array(
                'field' => COL_PASSWORD,
                'label' => COL_PASSWORD,
                'rules' => 'required|min_length[6]',
                'errors' => array(
                    'required' => 'Password harap diisi',
                    'min_length' => 'Password harus mengandung minimal {param} karakter'
                )
            ),
            array(
                'field' => 'RepeatPassword',
                'label' => 'Repeat Password',
                'rules' => 'required|matches[Password]',
                'errors' => array(
                    'required' => 'Ulangi Password harap diisi',
                    'matches' => 'Isi Ulangi Password sesuai Password'
                )
            )
        );
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()) {
            $this->db->where(COL_USERNAME, $ruser[COL_USERNAME]);
            if(!$this->db->update(TBL_USERS, array(COL_PASSWORD=>md5($this->input->post(COL_PASSWORD))))) {
                redirect(current_url()."?error=1");
            } else {
                redirect(site_url());
            }
        } else {
            $this->load->view("user/resetpassword", $data);
            return false;
        }
    }

    function import() {
        header('Content-Type: application/json');
        $token = "horasitdel";
        $posttoken = $this->input->post("token");
        $datapost = $_POST;
        if(!$posttoken || $posttoken != $token) {
            ShowJsonError("Parameter API tidak valid");
            return false;
        }

        $rules = $this->muser->rules(true, ROLEUSER);
        $this->form_validation->set_rules($rules);

        if($this->form_validation->run()){
            $userdata = array(
                COL_USERNAME => $this->input->post(COL_USERNAME),
                COL_PASSWORD => md5($this->input->post(COL_PASSWORD)),
                COL_ROLEID => ROLEUSER,
                COL_ISSUSPEND => false
            );
            $userinfo = array(
                COL_USERNAME => $this->input->post(COL_USERNAME),
                COL_EMAIL => $this->input->post(COL_EMAIL),
                COL_NAME => $this->input->post(COL_NAME),
                COL_IDENTITYNO => $this->input->post(COL_IDENTITYNO),
                COL_BIRTHDATE => $this->input->post(COL_BIRTHDATE)?date('Y-m-d', strtotime($this->input->post(COL_BIRTHDATE))):null,
                COL_RELIGIONID => $this->input->post(COL_RELIGIONID),
                COL_GENDER => $this->input->post(COL_GENDER),
                COL_ADDRESS => $this->input->post(COL_ADDRESS),
                COL_PHONENUMBER => $this->input->post(COL_PHONENUMBER),
                COL_EDUCATIONID => $this->input->post(COL_EDUCATIONID),
                COL_UNIVERSITYNAME => $this->input->post(COL_UNIVERSITYNAME),
                COL_FACULTYNAME => $this->input->post(COL_FACULTYNAME),
                COL_MAJORNAME => $this->input->post(COL_MAJORNAME),
                COL_ISGRADUATED => $this->input->post(COL_ISGRADUATED) ? $this->input->post(COL_ISGRADUATED) : false,
                COL_GRADUATEDDATE => ($this->input->post(COL_ISGRADUATED) && $this->input->post(COL_GRADUATEDDATE) ? date('Y-m-d', strtotime($this->input->post(COL_GRADUATEDDATE))) : null),
                COL_YEAROFEXPERIENCE => $this->input->post(COL_YEAROFEXPERIENCE),
                COL_RECENTPOSITION => $this->input->post(COL_RECENTPOSITION),
                COL_RECENTSALARY => $this->input->post(COL_RECENTSALARY),
                COL_EXPECTEDSALARY => $this->input->post(COL_EXPECTEDSALARY),
                COL_REGISTEREDDATE => date('Y-m-d')
            );

            $reg = $this->muser->register($userdata, $userinfo, null);
            if($reg) {
                ShowJsonSuccess("Berhasil.");
                return true;
            }
            else {
                ShowJsonError("Gagal.");
                return false;
            }

        }else{
            ShowJsonError("Parameter API tidak valid.");
            return false;
        }
    }

    function login_guest() {
        $rules = array(
            array(
                'field' => 'UserName',
                'label' => 'UserName',
                'rules' => 'required'
            ),
            array(
                'field' => 'Password',
                'label' => 'Password',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()){
            $username = $this->input->post(COL_USERNAME);
            $password = $this->input->post(COL_PASSWORD);

            if($this->muser->authenticate($username, $password)) {
                if($this->muser->IsSuspend($username)) {
                    ShowJsonError('Akun anda di suspend.');
                    return;
                }

                // Update Last Login IP
                $this->db->where(COL_USERNAME, $username);
                $this->db->update(TBL_USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));

                $userdetails = $this->muser->getdetails($username);
                SetLoginSession($userdetails);
                ShowJsonSuccess('Berhasil');
                return;
            }
            else {
                ShowJsonError('Username / password salah.');
                return;
            }
        } else {
            ShowJsonError('Harap isi username dan password.');
            return;
        }
    }
}
