<?php
class Management extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
        redirect('user/login');
    }
  }

  public function kelas() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('user/dashboard');
    }
    $kdTahunAjaran = $this->input->get(COL_KD_TAHUNAJARAN);
    if(empty($kdTahunAjaran)) {
      $rtahunajaran = $this->db
      ->where(COL_IS_ACTIVE, 1)
      ->get(TBL_MTAHUNAJARAN)
      ->row_array();

      if(!empty($rtahunajaran)) {
        $kdTahunAjaran = $rtahunajaran[COL_KD_TAHUNAJARAN];
      }
    }
    $data['title'] = "Manajemen Kelas";
    $data['kdTA'] = $kdTahunAjaran;
    $data['rkelas'] = $this->db
    ->select('mkelas.*, count(distinct tkelasmatapelajaran.Uniq) as count_mapel, count(distinct tkelaspelajar.Uniq) as count_pelajar, count(distinct tkelasjadwal.Uniq) as count_jadwal')
    ->join(TBL_TKELASMATAPELAJARAN,TBL_TKELASMATAPELAJARAN.".".COL_KD_KELAS." = ".TBL_MKELAS.".".COL_KD_KELAS." AND ".TBL_TKELASMATAPELAJARAN.'.'.COL_KD_TAHUNAJARAN."= $kdTahunAjaran","left")
    ->join(TBL_TKELASPELAJAR,TBL_TKELASPELAJAR.".".COL_KD_KELAS." = ".TBL_MKELAS.".".COL_KD_KELAS." AND ".TBL_TKELASPELAJAR.'.'.COL_KD_TAHUNAJARAN."= $kdTahunAjaran","left")
    ->join(TBL_TKELASJADWAL,TBL_TKELASJADWAL.".".COL_KD_KELAS." = ".TBL_MKELAS.".".COL_KD_KELAS." AND ".TBL_TKELASJADWAL.'.'.COL_KD_TAHUNAJARAN."= $kdTahunAjaran","left")
    ->group_by(TBL_MKELAS.".".COL_KD_KELAS)
    ->get(TBL_MKELAS)
    ->result_array();
    $this->load->view('management/form_class', $data);
  }

  public function matapelajaran($kdTA, $kdKelas) {
    $data = array(
      'kdTA' => $kdTA,
      'kdKelas' => $kdKelas
    );
    $this->load->view('management/_mapel_form', $data);
  }

  public function matapelajaran_partial($kdTA, $kdKelas) {
    $rmapel = $this->db
    ->join(TBL_MMATAPELAJARAN,TBL_MMATAPELAJARAN.".".COL_KD_MATAPELAJARAN." = ".TBL_TKELASMATAPELAJARAN.".".COL_KD_MATAPELAJARAN,"left")
    ->join(TBL_MPENGAJAR,TBL_MPENGAJAR.".".COL_KD_PENGAJAR." = ".TBL_TKELASMATAPELAJARAN.".".COL_KD_PENGAJAR,"left")
    ->where(COL_KD_TAHUNAJARAN, $kdTA)
    ->where(COL_KD_KELAS, $kdKelas)
    ->order_by(TBL_MMATAPELAJARAN.".".COL_NM_MATAPELAJARAN)
    ->get(TBL_TKELASMATAPELAJARAN)
    ->result_array();

    $this->load->view('management/_mapel_index', array('rmapel'=>$rmapel));
  }

  public function matapelajaran_add($kdTA, $kdKelas) {
    $user = GetLoggedUser();
    if(!empty($_POST)){
      $rcheck = $this->db
      ->where(array(
        COL_KD_MATAPELAJARAN => $this->input->post(COL_KD_MATAPELAJARAN),
        COL_KD_TAHUNAJARAN => $kdTA,
        COL_KD_KELAS => $kdKelas
      ))
      ->get(TBL_TKELASMATAPELAJARAN)
      ->row_array();

      if(!empty($rcheck)) {
        ShowJsonError("Mata Pelajaran sudah ada.");
        return false;
      }

      $data['data'] = $_POST;
      $data = array(
         COL_KD_MATAPELAJARAN => $this->input->post(COL_KD_MATAPELAJARAN),
         COL_KD_PENGAJAR => $this->input->post(COL_KD_PENGAJAR),
         COL_KD_TAHUNAJARAN => $kdTA,
         COL_KD_KELAS => $kdKelas,
         COL_CREATEDBY => $user[COL_USERNAME],
         COL_CREATEDON => date('Y-m-d H:i:s')
      );
      $res = $this->db->insert(TBL_TKELASMATAPELAJARAN, $data);
      if($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  function matapelajaran_delete($uniq){
    $res = $this->db->delete(TBL_TKELASMATAPELAJARAN, array(COL_UNIQ => $uniq));
    if($res) {
      ShowJsonSuccess("Berhasil");
    } else {
      ShowJsonError("Gagal");
    }
  }

  public function pelajar($kdTA, $kdKelas) {
    $data = array(
      'kdTA' => $kdTA,
      'kdKelas' => $kdKelas
    );
    $this->load->view('management/_pelajar_form', $data);
  }

  public function pelajar_partial($kdTA, $kdKelas) {
    $rpelajar = $this->db
    ->join(TBL_MPELAJAR,TBL_MPELAJAR.".".COL_KD_PELAJAR." = ".TBL_TKELASPELAJAR.".".COL_KD_PELAJAR,"left")
    ->where(COL_KD_TAHUNAJARAN, $kdTA)
    ->where(COL_KD_KELAS, $kdKelas)
    ->order_by(TBL_MPELAJAR.".".COL_NM_PELAJAR)
    ->get(TBL_TKELASPELAJAR)
    ->result_array();

    $this->load->view('management/_pelajar_index', array('rpelajar'=>$rpelajar));
  }

  public function pelajar_add($kdTA, $kdKelas) {
    $user = GetLoggedUser();
    if(!empty($_POST)){
      $rcheck = $this->db
      ->where(array(
        COL_KD_PELAJAR => $this->input->post(COL_KD_PELAJAR),
        COL_KD_TAHUNAJARAN => $kdTA
      ))
      ->get(TBL_TKELASPELAJAR)
      ->row_array();

      if(!empty($rcheck)) {
        ShowJsonError("Pelajar sudah dialokasikan.");
        return false;
      }

      $data['data'] = $_POST;
      $data = array(
         COL_KD_PELAJAR => $this->input->post(COL_KD_PELAJAR),
         COL_KD_TAHUNAJARAN => $kdTA,
         COL_KD_KELAS => $kdKelas,
         COL_CREATEDBY => $user[COL_USERNAME],
         COL_CREATEDON => date('Y-m-d H:i:s')
      );
      $res = $this->db->insert(TBL_TKELASPELAJAR, $data);
      if($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  function pelajar_delete($uniq){
    $res = $this->db->delete(TBL_TKELASPELAJAR, array(COL_UNIQ => $uniq));
    if($res) {
      ShowJsonSuccess("Berhasil");
    } else {
      ShowJsonError("Gagal");
    }
  }

  public function jadwal($kdTA=null, $kdKelas=null) {
    $_kdTA = $this->input->get(COL_KD_TAHUNAJARAN);
    $_kdKelas = $this->input->get(COL_KD_KELAS);
    if(!empty($_kdTA)) {
      $kdTA = $_kdTA;
    }
    if(!empty($_kdKelas)) {
      $kdKelas = $_kdKelas;
    }

    if(empty($kdTA)) {
      $rtahunajaran = $this->db
      ->where(COL_IS_ACTIVE, 1)
      ->get(TBL_MTAHUNAJARAN)
      ->row_array();

      if(!empty($rtahunajaran)) {
        $kdTA = $rtahunajaran[COL_KD_TAHUNAJARAN];
      }
    }

    if(empty($kdKelas)) {
      $kdKelas = -999;
    }

    $data = array(
      'title' => 'Manajemen Jadwal',
      'kdTA' => $kdTA,
      'kdKelas' => $kdKelas
    );

    $data['rjadwal'] = $this->db
    ->join(TBL_MMATAPELAJARAN,TBL_MMATAPELAJARAN.".".COL_KD_MATAPELAJARAN." = ".TBL_TKELASJADWAL.".".COL_KD_MATAPELAJARAN,"left")
    ->join(TBL_MSESI,TBL_MSESI.".".COL_KD_SESI." = ".TBL_TKELASJADWAL.".".COL_KD_SESI,"left")
    ->where(array(
      COL_KD_TAHUNAJARAN=>$kdTA,
      COL_KD_KELAS=>$kdKelas
    ))
    ->order_by(TBL_TKELASJADWAL.'.'.COL_NM_HARI)
    ->order_by(TBL_MSESI.'.'.COL_JAM_FROM)
    ->get(TBL_TKELASJADWAL)
    ->result_array();

    $data['rsesi'] = $this->db
    ->order_by(COL_JAM_FROM)
    ->get(TBL_MSESI)
    ->result_array();

    $this->load->view('management/form_jadwal', $data);
  }

  public function jadwal_add($kdTA, $kdKelas) {
    $user = GetLoggedUser();
    if(!empty($_POST)){
      $rcheck = $this->db
      ->where(array(
        COL_KD_TAHUNAJARAN => $kdTA,
        COL_KD_KELAS => $kdKelas,
        COL_NM_HARI => $this->input->post(COL_NM_HARI),
        COL_KD_SESI => $this->input->post(COL_KD_SESI)
      ))
      ->get(TBL_TKELASJADWAL)
      ->row_array();

      if(!empty($rcheck)) {
        ShowJsonError("Sesi sudah ada.");
        return false;
      }

      $data['data'] = $_POST;
      $data = array(
         COL_KD_TAHUNAJARAN => $kdTA,
         COL_KD_KELAS => $kdKelas,
         COL_NM_HARI => $this->input->post(COL_NM_HARI),
         COL_KD_SESI => $this->input->post(COL_KD_SESI),
         COL_KD_MATAPELAJARAN => $this->input->post(COL_KD_MATAPELAJARAN),
         COL_CREATEDBY => $user[COL_USERNAME],
         COL_CREATEDON => date('Y-m-d H:i:s')
      );
      $res = $this->db->insert(TBL_TKELASJADWAL, $data);
      if($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  function jadwal_delete($uniq){
    $res = $this->db->delete(TBL_TKELASJADWAL, array(COL_UNIQ => $uniq));
    if($res) {
      ShowJsonSuccess("Berhasil");
    } else {
      ShowJsonError("Gagal");
    }
  }

  public function nilai($kdTA=null, $kdKelas=null, $kdMapel=null) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEGURU) {
      redirect('user/dashboard');
    }

    $_kdTA = $this->input->get(COL_KD_TAHUNAJARAN);
    $_kdKelas = $this->input->get(COL_KD_KELAS);
    $_kdMapel = $this->input->get(COL_KD_MATAPELAJARAN);
    if(!empty($_kdTA)) {
      $kdTA = $_kdTA;
    }
    if(!empty($_kdKelas)) {
      $kdKelas = $_kdKelas;
    }
    if(!empty($_kdMapel)) {
      $kdMapel = $_kdMapel;
    }

    if(empty($kdTA)) {
      $rtahunajaran = $this->db
      ->where(COL_IS_ACTIVE, 1)
      ->get(TBL_MTAHUNAJARAN)
      ->row_array();

      if(!empty($rtahunajaran)) {
        $kdTA = $rtahunajaran[COL_KD_TAHUNAJARAN];
      }
    }

    if(empty($kdKelas)) {
      $kdKelas = -999;
    }
    if(empty($_kdMapel)) {
      $kdMapel = -999;
    }

    $data = array(
      'title' => 'Manajemen Nilai',
      'kdTA' => $kdTA,
      'kdKelas' => $kdKelas,
      'kdMapel' => $kdMapel
    );

    $data['rpelajar'] = $this->db
    ->join(TBL_MPELAJAR,TBL_MPELAJAR.".".COL_KD_PELAJAR." = ".TBL_TKELASPELAJAR.".".COL_KD_PELAJAR,"left")
    ->where(array(
      COL_KD_TAHUNAJARAN=>$kdTA,
      COL_KD_KELAS=>$kdKelas
    ))
    ->order_by(COL_NM_PELAJAR)
    ->get(TBL_TKELASPELAJAR)
    ->result_array();

    $data['rkomponen'] = $this->db
    ->order_by(COL_NM_KOMPONEN)
    ->get(TBL_MKOMPONEN)
    ->result_array();

    $this->load->view('management/form_nilai', $data);
  }

  public function nilai_input($kdTA, $kdKelas, $kdMapel, $kdKomponen, $kdPelajar) {
    $user = GetLoggedUser();

    $rkomponen = $this->db
    ->where(COL_KD_KOMPONEN, $kdKomponen)
    ->get(TBL_MKOMPONEN)
    ->row_array();

    if(empty($kdKomponen)) {
      ShowJsonError("Komponen tidak valid.");
      return false;
    }

    $cond = array(
      COL_KD_TAHUNAJARAN=>$kdTA,
      COL_KD_KELAS=>$kdKelas,
      COL_KD_MATAPELAJARAN=>$kdMapel,
      COL_KD_PELAJAR=>$kdPelajar,
      COL_NM_KOMPONEN=>$rkomponen[COL_NM_KOMPONEN]
    );

    $rnilai = $this->db
    ->where($cond)
    ->get(TBL_TNILAI)
    ->row_array();

    $res = false;
    if(empty($rnilai)) {
      $res = $this->db
      ->insert(TBL_TNILAI, array_merge($cond, array(
        COL_NM_KOMPONEN=>$rkomponen[COL_NM_KOMPONEN],
        COL_BOBOT=>$rkomponen[COL_BOBOT],
        COL_NILAI=>$this->input->post(COL_NILAI),
        COL_CREATEDBY=>$user[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      )));
    } else {
      $res = $this->db
      ->where($cond)
      ->update(TBL_TNILAI, array(
        COL_NM_KOMPONEN=>$rkomponen[COL_NM_KOMPONEN],
        COL_BOBOT=>$rkomponen[COL_BOBOT],
        COL_NILAI=>$this->input->post(COL_NILAI),
        COL_UPDATEDBY=>$user[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      ));
    }

    if($res) ShowJsonSuccess('Berhasil');
    else ShowJsonError('Gagal');
  }

  public function nilai_rekapitulasi($kdTA=null, $kdKelas=null, $kdPelajar=null) {
    $ruser = GetLoggedUser();
    $_kdTA = $this->input->get(COL_KD_TAHUNAJARAN);
    $_kdKelas = $this->input->get(COL_KD_KELAS);
    $_kdPelajar = $this->input->get(COL_KD_PELAJAR);
    if(!empty($_kdTA)) {
      $kdTA = $_kdTA;
    }
    if(!empty($_kdPelajar)) {
      $kdPelajar = $_kdPelajar;
    }
    if(!empty($_kdKelas)) {
      $kdKelas = $_kdKelas;
    }

    if(empty($kdTA)) {
      $rtahunajaran = $this->db
      ->where(COL_IS_ACTIVE, 1)
      ->get(TBL_MTAHUNAJARAN)
      ->row_array();

      if(!empty($rtahunajaran)) {
        $kdTA = $rtahunajaran[COL_KD_TAHUNAJARAN];
      }
    }

    if($ruser[COL_ROLEID] == ROLESISWA) {
      $mpelajar = $this->db
      ->where(COL_NM_NOMORINDUKSISWA, $ruser[COL_USERNAME])
      ->get(TBL_MPELAJAR)
      ->row_array();

      if(!empty($mpelajar)) {
        $rkelas = $this->db
        ->where(array(
          COL_KD_TAHUNAJARAN=>$kdTA,
          COL_KD_PELAJAR=>$mpelajar[COL_KD_PELAJAR]
        ))
        ->get(TBL_TKELASPELAJAR)
        ->row_array();

        if(!empty($rkelas)) {
          $kdKelas = $rkelas[COL_KD_KELAS];
        }
        $kdPelajar = $mpelajar[COL_KD_PELAJAR];
      }
    }

    $data = array(
      'title' => 'Rekapitulasi Nilai',
      'kdTA' => $kdTA,
      'kdKelas' => $kdKelas,
      'kdPelajar' => $kdPelajar
    );

    $data['rkomponen'] = $this->db
    ->order_by(COL_NM_KOMPONEN)
    ->get(TBL_MKOMPONEN)
    ->result_array();

    $this->load->view('management/form_nilai_rekapitulasi', $data);
  }

  public function materi($kdTA=null, $kdKelas=null, $kdMapel=null) {
    $ruser = GetLoggedUser();
    /*if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEGURU) {
      redirect('user/dashboard');
    }*/

    $_kdTA = $this->input->get(COL_KD_TAHUNAJARAN);
    $_kdKelas = $this->input->get(COL_KD_KELAS);
    $_kdMapel = $this->input->get(COL_KD_MATAPELAJARAN);
    if(!empty($_kdTA)) {
      $kdTA = $_kdTA;
    }
    if(!empty($_kdKelas)) {
      $kdKelas = $_kdKelas;
    }
    if(!empty($_kdMapel)) {
      $kdMapel = $_kdMapel;
    }

    if(empty($kdTA)) {
      $rtahunajaran = $this->db
      ->where(COL_IS_ACTIVE, 1)
      ->get(TBL_MTAHUNAJARAN)
      ->row_array();

      if(!empty($rtahunajaran)) {
        $kdTA = $rtahunajaran[COL_KD_TAHUNAJARAN];
      }
    }

    if(empty($kdKelas)) {
      $kdKelas = -999;
    }
    /*if(empty($_kdMapel)) {
      $kdMapel = -999;
    }*/

    if($ruser[COL_ROLEID] == ROLESISWA) {
      $mpelajar = $this->db
      ->where(COL_NM_NOMORINDUKSISWA, $ruser[COL_USERNAME])
      ->get(TBL_MPELAJAR)
      ->row_array();

      if(!empty($mpelajar)) {
        $rkelas = $this->db
        ->where(array(
          COL_KD_TAHUNAJARAN=>$kdTA,
          COL_KD_PELAJAR=>$mpelajar[COL_KD_PELAJAR]
        ))
        ->get(TBL_TKELASPELAJAR)
        ->row_array();

        if(!empty($rkelas)) {
          $kdKelas = $rkelas[COL_KD_KELAS];
        }
      }
    }

    $data = array(
      'title' => 'Manajemen Materi',
      'kdTA' => $kdTA,
      'kdKelas' => $kdKelas,
      'kdMapel' => $kdMapel
    );

    $cond = array(
      COL_KD_TAHUNAJARAN=>$kdTA,
      COL_KD_KELAS=>$kdKelas
    );
    if(!empty($kdMapel)) {
      $cond[COL_KD_MATAPELAJARAN] = $kdMapel;
    }
    if($ruser[COL_ROLEID] == ROLEGURU) {
      $cond[COL_CREATEDBY]=$ruser[COL_USERNAME];
    }
    $data['rmateri'] = $this->db
    ->where($cond)
    ->order_by(COL_CREATEDON, 'desc')
    ->get(TBL_TMATERI)
    ->result_array();

    $this->load->view('management/form_materi', $data);
  }

  public function materi_add($kdTA, $kdKelas, $kdMapel) {
    $ruser = GetLoggedUser();
    $data = array(
      COL_KD_TAHUNAJARAN=>$kdTA,
      COL_KD_KELAS=>$kdKelas,
      COL_KD_MATAPELAJARAN=>$kdMapel,
      COL_NM_JUDUL=>$this->input->post(COL_NM_JUDUL),
      COL_NM_KETERANGAN=>$this->input->post(COL_NM_KETERANGAN),
      COL_CREATEDBY => $ruser[COL_USERNAME],
      COL_CREATEDON => date('Y-m-d H:i:s')
    );

    $config['upload_path'] = MY_UPLOADPATH;
    $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
    $config['overwrite'] = TRUE;
    if(!empty($_FILES["userfile"]["name"])) {
      $uname = $ruser[COL_USERNAME];
      if(!empty($uname)) {
        $config['file_name'] = strtolower($uname)."-".date('Y-m-dHis').".".end((explode(".", $_FILES["userfile"]["name"])));
      }

      $this->load->library('upload',$config);
      if(!$this->upload->do_upload("userfile")){
        $err = $this->upload->display_errors();
        ShowJsonError(strip_tags($err));
        return;
      }

      $dataupload = $this->upload->data();
      if(!empty($dataupload) && $dataupload['file_name']) {
          $data[COL_NM_FILE] = $dataupload['file_name'];
      }
    }

    $res = $this->db->insert(TBL_TMATERI, $data);
    if($res) ShowJsonSuccess('Berhasil');
    else ShowJsonError('Gagal');
  }

  public function materi_delete($uniq) {
    $res = $this->db->where(COL_UNIQ, $uniq)->delete(TBL_TMATERI);
    if($res) {
      ShowJsonSuccess('Berhasil');
    } else {
      ShowJsonError('Gagal');
    }
  }
}
?>
