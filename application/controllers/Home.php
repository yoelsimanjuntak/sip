<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('mpost');
    }

    public function index()
    {
        /*if(!IsLogin()) {
            redirect('user/login');
        }
        redirect('user/dashboard');*/
        $data['title'] = 'Selamat Datang';
        $this->load->view('home/index', $data);
    }

    public function index_2() {
        $data['title'] = 'Beranda';
        $data['news'] = $this->mpost->search(4,"",1);
        $data['gallery'] = $this->mpost->search(4,"",4);
        $this->load->view('home/index_2', $data);
    }

    public function page($slug) {
      $data['title'] = 'Page';

      $this->db->join(TBL_POSTCATEGORIES,TBL_POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL_POSTS.".".COL_POSTCATEGORYID,"inner");
      $this->db->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = ".TBL_POSTS.".".COL_CREATEDBY,"inner");
      $this->db->where("(".TBL_POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL_POSTS.".".COL_POSTID." = '".$slug."')");
      $rpost = $this->db->get(TBL_POSTS)->row_array();
      if(!$rpost) {
          show_404();
          return false;
      }

      $this->db->where(COL_POSTID, $rpost[COL_POSTID]);
      $this->db->set(COL_TOTALVIEW, COL_TOTALVIEW."+1", FALSE);
      $this->db->update(TBL_POSTS);

      $data['title'] = $rpost[COL_POSTCATEGORYNAME];//strlen($rpost[COL_POSTTITLE]) > 50 ? substr($rpost[COL_POSTTITLE], 0, 50) . "..." : $rpost[COL_POSTTITLE];
      $data['data'] = $rpost;
      $this->load->view('home/page', $data);
    }

    public function post($cat) {
      $data['title'] = 'Tautan';

      $this->db->join(TBL_POSTCATEGORIES,TBL_POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL_POSTS.".".COL_POSTCATEGORYID,"inner");
      $this->db->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = ".TBL_POSTS.".".COL_CREATEDBY,"inner");
      $this->db->where(TBL_POSTS.".".COL_POSTCATEGORYID, $cat);
      $this->db->order_by(TBL_POSTS.".".COL_CREATEDON, 'desc');
      $data['res'] = $rpost = $this->db->get(TBL_POSTS)->result_array();

      if(!empty($rpost)) {
        $data['title'] = $rpost[0][COL_POSTCATEGORYNAME];
      }

      $data['data'] = $rpost;
      $this->load->view('home/post', $data);
    }

    function _404() {
        $this->load->view('home/error');
    }
}
