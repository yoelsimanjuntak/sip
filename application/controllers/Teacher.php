<?php
class Teacher extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
        redirect('user/login');
    }
  }

  public function index() {
    if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
      redirect('user/dashboard');
    }

    $data['title'] = "Pengajar";
    $data['res'] = $this->db->get(TBL_MPENGAJAR)->result_array();
    $this->load->view('teacher/index', $data);
  }

  public function add() {
    if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
      redirect('user/dashboard');
    }
    $user = GetLoggedUser();
    if(!empty($_POST)){
      $data['data'] = $_POST;
      $data = array(
        COL_NM_NOMORINDUKPEGAWAI => $this->input->post(COL_NM_NOMORINDUKPEGAWAI),
        COL_NM_PENGAJAR => $this->input->post(COL_NM_PENGAJAR),
        COL_NM_JENISKELAMIN => $this->input->post(COL_NM_JENISKELAMIN),

        COL_CREATEDBY => $user[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $res = $this->db->insert(TBL_MPENGAJAR, $data);
      if($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  public function edit($id) {
    if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
      redirect('user/dashboard');
    }
    $user = GetLoggedUser();
    if(!empty($_POST)){
      $data['data'] = $_POST;
      $data = array(
        COL_NM_NOMORINDUKPEGAWAI => $this->input->post(COL_NM_NOMORINDUKPEGAWAI),
        COL_NM_PENGAJAR => $this->input->post(COL_NM_PENGAJAR),
        COL_NM_JENISKELAMIN => $this->input->post(COL_NM_JENISKELAMIN),

        COL_UPDATEDBY => $user[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $res = $this->db->where(COL_KD_PENGAJAR, $id)->update(TBL_MPENGAJAR, $data);
      if($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  public function delete(){
    if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
      ShowJsonError("Not Authorized.");
      return;
    }
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      $this->db->delete(TBL_MPENGAJAR, array(COL_KD_PENGAJAR => $datum));
      $deleted++;
    }
    if($deleted){
      ShowJsonSuccess($deleted." data dihapus");
    }else{
      ShowJsonError("Tidak ada dihapus");
    }
  }
}
?>
